#include "FoucaultPendulum.h"
#include "qtglut.h"
#define _USE_MATH_DEFINES
#include <math.h>

foucaultPendulum::foucaultPendulum(){

	g = 9.81;
	m.acceleration = 0;
	m.accelerationZ = 0;
}

foucaultPendulum::~foucaultPendulum(){}

void foucaultPendulum::paint(){

	glColor3f(0,0,0);
	glBegin(GL_LINES);
		glVertex3f(0.0f,0.0f,0.0f);
		glVertex3f(m.global_position.x(),m.global_position.y(),m.globalZ);
	glEnd();

	glColor3f(0,1,0);
	glTranslatef(m.global_position.x(),m.global_position.y(),m.globalZ);
	glutSolidSphere(m.mass,10,8);
	glTranslatef(-(m.global_position.x()),-m.global_position.y(),-m.globalZ);

	glTranslatef(0,-(l+2*m.mass),0);
	glColor4f(1.0f,211/255,155/255,0.4f); 
	glBegin(GL_QUADS);    
		glVertex3f(-10.0f, 0.0f, 10.0f);
		glVertex3f( 10.0f, 0.0f, 10.0f);
		glVertex3f( 10.0f, 0.0f,-10.0f);
		glVertex3f(-10.0f, 0.0f,-10.0f);          
	glEnd();
	glColor3f(0,0,0);
	glBegin(GL_TRIANGLES);
		glVertex3f(-2.0,0.01,-2.0);
		glVertex3f(0.0,0.01,-3.0);
		glVertex3f(2.0,0.01,-2.0);
		
		glVertex3f(-2.0,0.01,-2.0);
		glVertex3f(-3.0,0.01,0.0);
		glVertex3f(-2.0,0.01,2.0);
		
		glVertex3f(2.0,0.01,-2.0);
		glVertex3f(3.0,0.01,0.0);
		glVertex3f(2.0,0.01,2.0);
		
		glVertex3f(-2.0,0.01,2.0);
		glVertex3f(0.0,0.01,3.0);
		glVertex3f(2.0,0.01,2.0);
	glEnd();

	glBegin(GL_LINE_LOOP);
		for(int i =0; i <= 300; i++){
			double angle = 2 * M_PI * i / 300;
			double x = cos(angle);
			double y = sin(angle);
			glVertex3d(x,0.01,y);
		}
	glEnd();

	glColor3f(0.2,0.2,0.5);
	glBegin(GL_LINES);
		glVertex3f(-2.0,0.01,2.0);
		glVertex3f(2.0,0.01,-2.0);

		glVertex3f(-2.0,0.01,-2.0);
		glVertex3f(2.0,0.01,2.0);
	glEnd();

	glTranslatef(0,l+2*m.mass,0);

	glColor4f(0.2f,0.4f,0.8f,0.8f); 
	glBegin(GL_LINES);
		for(int i = 0; i < path.size()-1; i++){
			glVertex3f(path[i][0],path[i][1],path[i][2]);
			glVertex3f(path[i+1][0],path[i+1][1],path[i+1][2]);
		}
	glEnd();
}

void foucaultPendulum::reset(){
	
	m.global_position.setY(-sqrt(pow(l+m.mass/2,2)-pow(m.global_position.x(),2)-pow(m.globalZ,2)));
	
	path.clear();
	std::vector<float> pos;
	pos.insert(pos.end(),m.global_position.x());
	pos.insert(pos.end(),m.global_position.y());
	pos.insert(pos.end(),m.globalZ);
	path.insert(path.end(), pos);
}

void foucaultPendulum::step(){

	float omega = (2*M_PI)/T*sin(lat);

	switch(integrator){
	//Euler
	case 0:
		m.acceleration = 2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*m.global_position.x();
		m.tang_vel = m.tang_vel+m.acceleration*dt;
		m.global_position.setX(m.global_position.x()+m.tang_vel*dt);
		m.accelerationZ = -2*omega*m.tang_vel+(pow(omega,2)-(g/l))*m.globalZ;
		m.tang_velZ = m.tang_velZ + m.accelerationZ*dt;
		m.globalZ = m.globalZ + m.tang_velZ*dt;
		break;
	//Backward-Euler
	case 1:
		m.global_position.setX(-(m.global_position.x()+2*omega*m.tang_velZ*pow(dt,2)+m.tang_vel*dt)/(((pow(omega,2)-g/l)*pow(dt,2))-1));
		m.globalZ = -(m.globalZ+2*omega*m.tang_vel*pow(dt,2)+m.tang_velZ*dt)/(((pow(omega,2)-g/l)*pow(dt,2))-1);

		m.acceleration = 2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*m.global_position.x();
		m.tang_vel = m.tang_vel+m.acceleration*dt;
		m.accelerationZ = -2*omega*m.tang_vel+(pow(omega,2)-(g/l))*m.globalZ;
		m.tang_velZ = m.tang_velZ + m.accelerationZ*dt;
		break;
	//Runge-Kutta
	case 2:
		m.acceleration = 2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*m.global_position.x();
		m.tang_vel = m.tang_vel+m.acceleration*dt;

		m.accelerationZ = -2*omega*m.tang_vel+(pow(omega,2)-(g/l))*m.globalZ;
		m.tang_velZ = m.tang_velZ + m.accelerationZ*dt;

		float A;
		float a;
		float B;
		float b;
		float C;
		float c;

		A = m.global_position.x()+(dt/2)*m.tang_vel;
		a = m.tang_vel+(2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*A)*(dt/2);
		B = m.global_position.x()+(dt/2)*a;
		b = m.tang_vel+(2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*B)*(dt/2);
		C = m.global_position.x()+dt*b;
		c = m.tang_vel+(2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*C)*dt;

		m.global_position.setX(m.global_position.x()+(dt/6)*(m.tang_vel+2*(a+b)+c));

		A = m.globalZ+(dt/2)*m.tang_velZ;
		a = m.tang_velZ+(2*omega*m.tang_vel+(pow(omega,2)-(g/l))*A)*(dt/2);
		B = m.globalZ+(dt/2)*a;
		b = m.tang_velZ+(2*omega*m.tang_vel+(pow(omega,2)-(g/l))*B)*(dt/2);
		C = m.globalZ+dt*b;
		c = m.tang_velZ+(2*omega*m.tang_vel+(pow(omega,2)-(g/l))*C)*dt;

		m.globalZ = m.globalZ+(dt/6)*(m.tang_velZ+2*(a+b)+c);

		break;
	//Verlet
	case 3:
		float ax;
		ax = 2*omega*m.tang_velZ+(pow(omega,2)-(g/l))*m.global_position.x();
		float az;
		az = -2*omega*m.tang_vel+(pow(omega,2)-(g/l))*m.globalZ;

		m.global_position.setX(m.global_position.x()+dt*m.tang_vel+(pow(dt,2)/2)*m.acceleration);
		m.globalZ = m.globalZ+dt*m.tang_velZ+(pow(dt,2)/2)*m.accelerationZ;

		m.tang_vel = m.tang_vel+(dt/2)*(ax+m.acceleration);
		m.tang_velZ = m.tang_velZ+(dt/2)*(az+m.accelerationZ);

		m.accelerationZ = az;
		m.acceleration = ax;	
		break;
	default:
		break;
	}
	
	m.global_position.setY(-sqrt(pow(l+m.mass/2,2)-pow(m.global_position.x(),2)-pow(m.globalZ,2)));
	std::vector<float> pos;
	pos.insert(pos.end(),m.global_position.x());
	pos.insert(pos.end(),m.global_position.y());
	pos.insert(pos.end(),m.globalZ);
	path.insert(path.end(), pos);

	emit callUpdate();
}