m = 1;
ka = 10^2;
kb = 10^10;
x0 = 1;
w= sqrt(ka/m)-10^(-10);

%K = [ka+kb, -kb; -kb, ka+kb];
K = ka+kb, -kb;
%G=@(t) [ka*x0*cos(w*t);0];
G=@(t) ka * x0 * cos(w*t);

x_n01 = 0;
x_n = 0;
v_n = 0;

t=0;
dt = 0.01;

stepsize = 0.1;
max = 1;
x = 0:stepsize:max;
y_x =x;
y_v = x;

%=======================================================================
% variational integrators
%=======================================================================
count =1;

for i=0:stepsize:max
    x_n1 = ( (4/(dt^2))*x_n - (4/dt)*v_n - 2*G(t) - K*x_n ) / (K - 4/(dt^2))
    x_v1 = -dt/v_n * ((x_n+x_n1)/2 + G(t));
    
    v_n = v_n1;
    x_n = x_n1;
    
    y_x(count) = x_n;
    y_v(count) = v_n;
    t = t+dt;
end

plot(x,y_x)
plot(x,y_v)
hold on;

%=======================================================================
% exponential integrator
%=======================================================================

% filter functions
psi =@(x) sinc(x)^2;
psi0 =@(x) cos(x)*sinc(x);
psi1 =@(x) sinc(x);
phi =@(x) sinc(x);

count =1;
for i=0:stepsize:max
    O_n = sqrt(K(1));
    g = @(xn) (G(xn)-K(1)*x0 ) ;
    
    tmp = inv( sqrt(K) );
    O_inv = tmp(1,:);
    
    x_n1 = cos(dt * O_n)*x_n + inv(O_n) * sin(dt*O_n)*v_n + (dt^2)/2 * psi(dt*O_n)*g(phi(dt*O_n)*x_n)
    v_n1 = -O_n*sin(dt*O_n)*x_n + cos(dt*O_n)*v_n

    x_n01 = x_n;
    
    v_n = v_n1
    x_n = x_n1
   
    y_x(count) = x_n;
    y_v(count) = v_n;
    count = count +1;
end

%plot(x,y_x)
%plot(x,y_v)
hold on;

%=======================================================================
% analytical solution
%=======================================================================

w1 = ka/m;
w2 = (ka+2*kb)/m;
w3 = (ka+kb)/m;

amp = (ka/m) * x0 * (1/(w1 - w*w)) * (1/(w2-w*w))*[w3-w*w; w3-w1];
pfu_x = @(t) amp*cos(w*t);
pfu_v = @(t) -w*amp*sin(w*t);

i = 0:0.002:2;
j = pfu_x(i);
k = pfu_v(i);

%plot(i,j,'r')
%plot(i,k,'g')



