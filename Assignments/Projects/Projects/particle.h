#ifndef PARTICLE_H
#define PARTICLE_H

#include <QWidget>

class Particle
{
public:
	Particle();
	Particle(float t);

	void setPosition(float d);
	void setVeloctiy(QPointF d);
	void setGlobalPos(QPointF d);
	void setTangVel(float d);

	float acceleration;
	float accelerationZ;
	float position;
	float positionZ;
	float globalZ;
	QPointF global_position;
	QPointF velocity;
	float tang_vel;
	float tang_velZ;
	QPolygonF curve_pos;
	QPolygonF curve_vel;
	float mass;


};

#endif // PARTICLE_H
