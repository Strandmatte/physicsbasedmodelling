#ifndef EXPORTHGEO_H
#define EXPORTHGEO_H

#include "../snowParticle.h"
#include <Eigen/Eigen>
#include <vector>

using namespace std;

class ExportHGeo
{
public:
	ExportHGeo(string filename, vector<snowParticle> particlesIn, int frame);
	//ExportHGeo(int test, int frame);
	~ExportHGeo(void);

private:
	vector<snowParticle>      particles;
	int                       pointCount;
	string                    pointPositions();
	string                    pointVelocity();
	string                    header();
	string                    attributes();
	string                    footer();
};



#endif // EXPORTHGEO_H
