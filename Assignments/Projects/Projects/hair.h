#pragma once
#include "hairNode.h"
#include "hairspring.h"
#include <vector>

class hair
{
public:
	hair();
	~hair();
	void                                  init();

	vector<hairNode>                      nodes;
	vector<HairSpring>                    springs;

	int                                   subdiv;

	vector<float>                         position;
	vector<float>                         rotation;

};

