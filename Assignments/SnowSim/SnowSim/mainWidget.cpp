//Im Debug-Modus m�ssen Pfade angepasst werden...
#ifdef _DEBUG
#define DEBUG_IF(cond) if(cond)
#else
#define DEBUG_IF(cond) if(false)
#endif

#include "mainWidget.h"

mainWidget::mainWidget(QWidget *parent) : QWidget(parent){

	//nur w�hrend debugging wegen den Pfaden...
	DEBUG_IF(true){
		WCHAR pbuff[MAX_PATH] = {0};
		GetModuleFileName(NULL, pbuff, MAX_PATH);
		std::wstring wpath = std::wstring(pbuff);
		std::string path(wpath.begin(),wpath.end());
		path = path.substr(0, path.find_last_of("\\/"));

		int len;
		int slength = (int)path.length() + 1;
		len = MultiByteToWideChar(CP_ACP, 0, path.c_str(), slength, 0, 0); 
		wchar_t* buf = new wchar_t[len];
		MultiByteToWideChar(CP_ACP, 0, path.c_str(), slength, buf, len);
		std::wstring r(buf);
		delete[] buf;

		SetCurrentDirectory(r.c_str());

		DWORD cchCurDir = MAX_PATH;
		wchar_t szCurDir[MAX_PATH];
		
		GetCurrentDirectory(cchCurDir, szCurDir);
	}

    setVideoHtml();

	createGridGroupBox();

	setLayout(layout);
	setWindowTitle("Snow Simulation");

	reset();
}

mainWidget::~mainWidget(){
	glWindow->~OGLWidget();
	label->~QLabel();
	startBtn->~QPushButton();
	stopBtn->~QPushButton();
}

void mainWidget::createGridGroupBox(){
	
	layout = new QGridLayout();
	layout->setColumnStretch(0,8);
	layout->setColumnStretch(1,1);
	layout->setColumnStretch(2,1);
	
	glWindow = new OGLWidget(this);

	layout->addWidget(vid,0,1,1,2,Qt::AlignCenter);

	paperBtn = new QPushButton();
	paperBtn->setText("Paper");

	specsBtn = new QPushButton();
	specsBtn->setText("Tech-Report");

	startBtn = new QPushButton();
	startBtn->setText("start");

	stopBtn = new QPushButton();
	stopBtn->setText("stop");

	resetBtn = new QPushButton();
	resetBtn->setText("reset");

	label = new QLabel(this);

	label->setWordWrap(true);

	label->setText(QString("R: reset Camera"));
	
	comboPropLabel = new QLabel();
	comboProp = new QComboBox();

	prop1Label = new QLabel();
	prop1 = new QLineEdit();
	prop2Label = new QLabel();
	prop2 = new QLineEdit();
	prop3Label = new QLabel();
	prop3 = new QLineEdit();
	prop4Label = new QLabel();
	prop4 = new QLineEdit();
	prop5Label = new QLabel();
	prop5 = new QLineEdit();
	prop6Label = new QLabel();
	prop6 = new QLineEdit();
	prop7Label = new QLabel();
	prop7 = new QLineEdit();
	
	comboPropLabel->setText("Simulation");
	prop1Label->setText("init Young's Mod (*10^5)");
	prop2Label->setText("crit Compression (*10^(-2))");
	prop3Label->setText("crit Stretch (*10^(-3))");
	prop4Label->setText("Hardening");
	prop5Label->setText("init. density (*10^2)");
	prop6Label->setText("Poisson's ratio");
	prop7Label->setText("timer in ms");

	comboProp->addItem("2D-Snowball");
	comboProp->addItem("3D-Snowball");
	comboProp->setCurrentIndex(0);
	prop1->setText("1.4");
	prop2->setText("2.5");
	prop3->setText("7.5");
	prop4->setText("10");
	prop5->setText("4");
	prop6->setText("0.2");
	prop7->setText("25");

	layout->addWidget(glWindow,0,0,-1,1);
	
	layout->addWidget(paperBtn,1,1,1,1,Qt::AlignCenter);
	layout->addWidget(specsBtn,1,2,1,1,Qt::AlignCenter);
	layout->addWidget(comboPropLabel,2,1,1,1,Qt::AlignRight);
	layout->addWidget(comboProp,2,2,1,1,Qt::AlignLeft);
	layout->addWidget(prop1Label,3,1,1,1,Qt::AlignRight);
	layout->addWidget(prop1,3,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop2Label,4,1,1,1,Qt::AlignRight);
	layout->addWidget(prop2,4,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop3Label,5,1,1,1,Qt::AlignRight);
	layout->addWidget(prop3,5,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop4Label,6,1,1,1,Qt::AlignRight);
	layout->addWidget(prop4,6,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop5Label,7,1,1,1,Qt::AlignRight);
	layout->addWidget(prop5,7,2,1,1,Qt::AlignLeft);		
	layout->addWidget(prop6Label,8,1,1,1,Qt::AlignRight);
	layout->addWidget(prop6,8,2,1,1,Qt::AlignLeft);		
	layout->addWidget(prop7Label,9,1,1,1,Qt::AlignRight);
	layout->addWidget(prop7,9,2,1,1,Qt::AlignLeft);		
	layout->addWidget(startBtn,10,1,1,2,Qt::AlignCenter);	
	layout->addWidget(stopBtn,11,1,1,2,Qt::AlignCenter);	
	layout->addWidget(resetBtn,12,1,1,2,Qt::AlignCenter);
	layout->addWidget(label,13,1,1,2,Qt::AlignCenter);

	layout->setRowStretch(0,8);
	layout->setRowStretch(1,1);
	layout->setRowStretch(2,1);
	layout->setRowStretch(3,1);
	layout->setRowStretch(4,1);
	layout->setRowStretch(5,1);
	layout->setRowStretch(6,1);
	layout->setRowStretch(7,1);
	layout->setRowStretch(8,1);
	layout->setRowStretch(9,1);
	layout->setRowStretch(10,1);
	layout->setRowStretch(11,1);
	layout->setRowStretch(12,1);
	layout->setRowStretch(13,8);

	connect(startBtn,SIGNAL(clicked()),this,SLOT(start()));
	connect(stopBtn,SIGNAL(clicked()),this,SLOT(stop()));
	connect(resetBtn,SIGNAL(clicked()),this,SLOT(reset()));
	connect(paperBtn,SIGNAL(clicked()),this,SLOT(openPaper()));
	connect(specsBtn,SIGNAL(clicked()),this,SLOT(openSpecs()));
	connect(comboProp,SIGNAL(currentIndexChanged(int)),this,SLOT(switchSim(int)));

	glWindow->setFocus();
}

void mainWidget::setVideoHtml(){
	
	vid = new QWebView(this);

    QWebSettings::globalSettings()->setAttribute(QWebSettings::PluginsEnabled, true);
    QWebSettings::globalSettings()->setAttribute(QWebSettings::AutoLoadImages, true);

	QString htmlStr = QString("<html>\n")
					+ QString("	<head>\n")
					+ QString("    <title>Disney-Video</title>\n")
					+ QString("    <style TYPE=\"text/css\">\n")
					+ QString("        body {background-color:white;}\n")
					+ QString("        div{width:100%; height:100%; float: left; padding:0px; margin:0px;}\n")
					+ QString("    </style>\n")
					+ QString("	</head>\n")
					+ QString("	<body>\n")
					+ QString("    <div>\n")
					+ QString("        <iframe class=\"youtube-player\" type=\"text/html\" width=\"100%\" height=\"100%\" src=\"http://www.youtube.com/embed/9H1gRQ6S7gg\" allowfullscreen frameborder=\"0\">\n")
					+ QString("		</iframe>\n")
					+ QString("    </div>\n")
					+ QString("	</body>\n")
					+ QString("</html>");

	vid->setHtml(htmlStr);
}

void mainWidget::mousePressEvent(QMouseEvent *event){
	glWindow->mousePressEvent(event);
}

void mainWidget::mouseReleaseEvent(QMouseEvent *event){
	glWindow->mouseReleaseEvent(event);
}

void mainWidget::mouseMoveEvent(QMouseEvent *event){

}

void mainWidget::wheelEvent(QWheelEvent *event){
	glWindow->wheelEvent(event);
}

void mainWidget::keyPressEvent(QKeyEvent *event){

	switch(event->key()) {
	case Qt::Key_Up:
	case Qt::Key_Down:
	break;
	default:
	break;
	}
}

void mainWidget::start(){
	glWindow->timer.start(prop7->text().toInt());
	
	reset();
	glWindow->setFocus();
}

void mainWidget::stop(){
	glWindow->timer.stop();
	glWindow->setFocus();
}

void mainWidget::reset(){
	
	glWindow->snow->E0 = prop1->text().toFloat()*pow(10,5);
	glWindow->snow->thetaC = prop2->text().toFloat()*pow(10,-2);
	glWindow->snow->thetaS = prop3->text().toFloat()*pow(10,-3);
	glWindow->snow->xi = prop4->text().toFloat();
	glWindow->snow->rho0 = prop5->text().toFloat()*pow(10,2);
	glWindow->snow->nu = prop6->text().toFloat();

	glWindow->snow->reset();

	glWindow->makeUpdate();
	glWindow->setFocus();
}

void mainWidget::openPaper(){
	system("start ../../Documents/snow.pdf");
}

void mainWidget::openSpecs(){
	system("start ../../Documents/snow-tech-report.pdf");
}

void mainWidget::switchSim(int i){
	stop();
	glWindow->snow->switchSim(i);
	reset();
}
