#ifndef HAIRSPRING_H
#define HAIRSPRING_H

#include <vector>
#include "hairNode.h"

class HairSpring
{
public:
	HairSpring();
	~HairSpring();

	float                            zeroDist;
	float                            elong;
	float                            elong_x;
	float                            elong_y;
	float                            elong_z;

	hairNode*                        leftNode;
	hairNode*                        rightNode;

	void                             setSpring(hairNode* n1, hairNode* n2);
	void                             setZero();
	void                             update();
	void                             dampen();
	float                            getDistance();
	float                            getDistanceX();
	float                            getDistanceY();
	float                            getDistanceZ();
};

#endif // HAIRSPRING_H
