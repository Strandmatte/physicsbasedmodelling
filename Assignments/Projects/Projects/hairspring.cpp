#include "hairspring.h"


HairSpring::HairSpring(){

}

HairSpring::~HairSpring()
{

}


void HairSpring::setSpring(hairNode *n1, hairNode *n2)
{
	leftNode = n1;
	rightNode = n2;
	setZero();
	update();
}


void HairSpring::setZero()
{
	zeroDist = getDistance();
}



void HairSpring::update()
{
	double norm = getDistance();

	elong =   norm - zeroDist;


	float dx = getDistanceX();
	float dy = getDistanceY();
	float dz = getDistanceZ();

	elong_x = dx-(dx/norm)*zeroDist;
	elong_y = dy-(dy/norm)*zeroDist;
	elong_z = dz-(dz/norm)*zeroDist;

}

float HairSpring::getDistance()
{
	return sqrt(
				pow(rightNode->pos.at(0)-leftNode->pos.at(0),2) +
				pow(rightNode->pos.at(1)-leftNode->pos.at(1),2) +
				pow(rightNode->pos.at(2)-leftNode->pos.at(2),2)
			);
}

float HairSpring::getDistanceX()
{
	return rightNode->pos.at(0)-leftNode->pos.at(0);
}
float HairSpring::getDistanceY()
{
	return rightNode->pos.at(1)-leftNode->pos.at(1);
}
float HairSpring::getDistanceZ()
{
	return rightNode->pos.at(2)-leftNode->pos.at(2);
}

