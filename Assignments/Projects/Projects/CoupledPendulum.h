#pragma once
#ifndef COUPLEDPENDULUM_H
#define COUPLEDPENDULUM_H

#include <QWidget>
#include <vector>
#include <QtOpenGL/QGLWidget>
#include "particle.h"

using namespace std;

class coupledPendulum : public QObject{

	Q_OBJECT

public:
	coupledPendulum();
	~coupledPendulum();

	void setAbsPos();
	void paint();

	float dt;
	float g;
	float l1;
	float l2;
	float sl;
	float k;

	Particle m1;
	Particle m2;

	public slots:
		void reset();
		void step();

signals:
		void callUpdate();

};

#endif //COUPLEDPENDULUM_H