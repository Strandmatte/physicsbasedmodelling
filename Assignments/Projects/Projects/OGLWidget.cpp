#include "OGLWidget.h"
#include <GL/gl.h>
#include <GL/glu.h>
#define _USE_MATH_DEFINES
#include "qtglut.h"
#include <qlabel.h>

OGLWidget::OGLWidget(QWidget *parent) : QGLWidget(parent) {

	p = 0;

	eye[0] = eye[0] = 0.0;
	eye[1] = eye[1] = 1.0;
	eye[2] = eye[2] = 15.0;
	at[0] = at[0] = 0.0;
	at[1] = at[1] = 0.0;
	at[2] = at[2] = 0.0;
	up[0] = up[0] = 0.0;
	up[1] = up[1] = 1.0;
	up[2] = up[2] = 0.0;
	znear = 1.0;
	zfar = 100.0;
	WperH = 1.0;

	rot[0] = 0.0;
	rot[1] = 0.0;

	trans[0] = trans[0] = 0.0;
	trans[1] = trans[1] = 0.0;
	trans[2] = trans[2] = 0.0;

	mPressedL = mPressedM = mPressedR = animating = false;

	t1 = new Tautochrone(0.1);
	t2 = new Tautochrone(M_PI/2);	
	t3 = new Tautochrone(M_PI/4);

	connect(t1,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
	connect(t2,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
	connect(t3,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
	connect(&dp,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
	connect(&cp,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
	connect(&fp,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
	connect(&hb,SIGNAL(callUpdate()),this,SLOT(makeUpdate()));
}

OGLWidget::~OGLWidget(){


}

void OGLWidget::initializeGL() {
	glClearColor (1.0, 1.0, 1.0, 1.0);
	glShadeModel (GL_SMOOTH);
	glClearDepth( 1.0);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
}

void OGLWidget::resizeGL(int w, int h) {
	glViewport (0, 0, (GLsizei) w, (GLsizei) h); 
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	WperH = (float)w/(float)h;
	gluPerspective(60.0, WperH, znear, zfar);
}

void OGLWidget::paintGL() {

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt (eye[0], eye[1], eye[2], at[0], at[1], at[2], up[0], up[1], up[2]);

	glTranslatef(0.0,0.0,trans[2]);
	glTranslatef(trans[0],trans[1],0.0);
	glRotatef(rot[0],1.0,0.0,0.0);
	glRotatef(rot[1],0.0,1.0,0.0);

	glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND); //Enable blending.
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //Set blending function.

	glClear (GL_COLOR_BUFFER_BIT);
	glClear (GL_DEPTH_BUFFER_BIT);

	switch(p){
	case 0:
		hb.paint();
		break;
	case 1:
		glTranslatef(0,5,0);
		fp.paint();
		break;
	case 2:
		glTranslatef(0,5,0);
		cp.paint();
		break;
	case 3:
		glTranslatef(0,5,0);
		dp.paint();
		break;
	case 4:
		t1->paint();
		//glTranslatef(0,0,5);
		t2->paint();
		//glTranslatef(0,0,-10);
		t3->paint();
		//glTranslatef(0,0,5);
		break;
	default:
		break;
	}

	if(p>0){
		glColor4f(0.6f,0.6f,1.0f,0.2f); 
		glBegin(GL_QUADS);    
			glVertex3f(-10.0f, 0.0f, 10.0f);
			glVertex3f( 10.0f, 0.0f, 10.0f);
			glVertex3f( 10.0f, 0.0f,-10.0f);
			glVertex3f(-10.0f, 0.0f,-10.0f);          
		glEnd();
	}
}

void OGLWidget::mousePressEvent(QMouseEvent *event){

	switch(event->button()){
	case Qt::LeftButton:
		mPressedL = true;
		mpos[0] = event->pos().x();
		mpos[1] = event->pos().y();
	break;
	case Qt::MidButton:
		mPressedM = true;
		mpos[0] = event->pos().x();
		mpos[1] = event->pos().y();
	break;
	case Qt::RightButton:
		mPressedR = true;
		mpos[0] = event->pos().x();
		mpos[1] = event->pos().y();
	break;
	}
}

void OGLWidget::mouseReleaseEvent(QMouseEvent *event){

	switch(event->button()){
	case Qt::LeftButton:
		mPressedL = false;
	break;
	case Qt::MidButton:
		mPressedM = false;
	break;
	case Qt::RightButton:
		mPressedR = false;
	break;
	}

}

void OGLWidget::mouseMoveEvent(QMouseEvent *event){

	if (mPressedM || mPressedL){

		rot[0] += (float)(event->pos().y()-mpos[1])/10;
		rot[1] += (float)(event->pos().x()-mpos[0])/10;

		mpos[0] = event->pos().x();
		mpos[1] = event->pos().y();

		updateGL();
	}
	else if (mPressedR){

		trans[0] += (float)(event->pos().x()-mpos[0])/10;
		trans[1] += -(float)(event->pos().y()-mpos[1])/10;

		mpos[0] = event->pos().x();
		mpos[1] = event->pos().y();

		updateGL();
	}
}

void OGLWidget::wheelEvent(QWheelEvent *event){

	trans[2] += (float)(event->delta()/100);

	updateGL();
}

void OGLWidget::keyPressEvent(QKeyEvent *event){
	switch(event->key()) {
	case Qt::Key_R:
		rot[0] = 0.0;
		rot[1] = 0.0;
		trans[0] = 0.0;
		trans[1] = 0.0;
		trans[2] = 0.0;
		updateGL();
	break;
	case Qt::Key_O:
	break;
	case Qt::Key_Left:
	break;
	case Qt::Key_Right:
	break;
	case Qt::Key_Up:
	break;
	case Qt::Key_Down:
	break;
	case Qt::Key_A:
	break;
	case Qt::Key_PageDown:
	break;
	case Qt::Key_PageUp:
	break;
	default:	
	break;
   }

	event->setAccepted(false);

}

void OGLWidget::makeUpdate(){
	updateGL();
}
