#pragma once
#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <QtOpenGL/QGLWidget>
#include <QtGui>
#include <vector>
#include "snowsim.h"

class OGLWidget : public QGLWidget{

    Q_OBJECT // must include this if you use Qt signals/slots

public:
	snowSim* snow;

	float eye[3];
	float at[3];
	float up[3];

	float trans[3];
	float rot[2];

	float znear;
	float zfar;
	float WperH;

	int mpos[2];

	bool mPressedL;
	bool mPressedM;
	bool mPressedR;

	bool animating;

	QTimer timer;

    OGLWidget(QWidget *parent = NULL);
	~OGLWidget(void);

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);

	public slots:
		void makeUpdate();
	signals:

protected:

};

#endif // OGLWIDGET_H
