#pragma once
#ifndef SNOWOBJECT_H
#define SNOWOBJECT_H

#include "snowParticle.h"
#include <Eigen/Eigen>
#include <vector>

using namespace std;

class snowObject{

public:
	snowObject(int t=0);
	~snowObject();

	vector<snowParticle>                particles;
	float								x;
	void                                snowball2D();
	void                                snowball3D();

protected:
};

#endif //SNOWOBJECT_H
