#include "snowSim.h"
#include <iostream>
#include "snowGrid.h"
#include "houdini/ExportHGeo.h"
#include "SnowParticle.h"
#include "QDebug"
/*
*  snowsim calls the snow Solver per step and outputs the scene
*/

snowSim::snowSim(){
	vector<snowObject*> snowObj;
	snowObj.push_back(new snowObject());
	solver = new snowSolver(snowObj);

	timeStep = 1.0;
	time = 0.0;
	frame = 0;
}

snowSim::~snowSim(){

}

void snowSim::reset(){
	solver->setDensities(rho0);
}

void snowSim::paint(){

	p = solver->getSnowObject(0);

	subdiv = solver->grid->getSubdiv();
	csize = solver->grid->getCellsize();

	// paint Particles
	for(int i = 0; i < p->particles.size(); i++)
	{
		pos = p->particles[i].getPosition();
		glColor4f(0.9,0.2,0.2,1);

		glPushMatrix();
			glTranslatef(pos(0),pos(1), pos(2));
			glutSolidSphere(p->particles[i].getMass(),10,10);
		glPopMatrix();
	}
	
	//Move Grid to overlay Particles
	glTranslatef(solver->minPos(0),solver->minPos(1),solver->minPos(2));
	
	// paint Grid
	for(int i=0; i<subdiv(0); i++){
		for(int j=0; j<subdiv(1); j++){
			for(int k=0; k<subdiv(2); k++){
				glPushMatrix();
					glColor3f(float(i)/subdiv(0),float(j)/subdiv(1),float(k)/subdiv(2));
					glTranslatef(csize(0)*i,csize(1)*j,csize(2)*k);
					glutWireCube(csize(0));
				glPopMatrix();
			}
		}
	}
	glTranslatef(-solver->minPos(0),-solver->minPos(1),-solver->minPos(2));
}


void snowSim::step(){

	// Perform Solver Step
	solver->update(timeStep);

	// Export Geo File
	ExportHGeo("testfile" , solver->getSnowObject(0)->particles, frame);

	// Update Time
	frame += 1;
	time += timeStep;
}

void snowSim::switchSim(int i){
	vector<snowObject*> snowObj;
	switch(i){
	case 0:
	case 1:
		snowObj.push_back(new snowObject(i));
		break;
	default:
		break;
	}
	solver->refresh(snowObj);
}


void snowSim::paintParticles(){

}

void snowSim::paintGrid(){

}

void snowSim::paintVel(){

}

void snowSim::paintMass(){

}
