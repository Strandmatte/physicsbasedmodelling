#include "hairball.h"
#include "qdebug.h"
#define PI 3.14159265

hairball::hairball()
{
	qDebug() << "========= constr";

	g=-9.81;
	k = 20000;
	init();

	qDebug() << "========= constr end";
}

hairball::~hairball()
{

}


void hairball::init()
{

	qDebug() << "========= init";

	counter = 0;
	direction = 1;

	// create a few hair fibres
	float radius = 3;
	for(int i=-46; i<46; i+=2)
	{
		hair* h1 = new hair();
		fibres.push_back(*h1);
		
		float rot = 2*i*PI/180;

		fibres.at((i+46)/2).position[0] = cos(rot)/radius;
		fibres.at((i+46)/2).position[1] = 0;
		fibres.at((i+46)/2).position[2] = sin(rot)/radius;
		fibres.at((i+46)/2).rotation[1] = rot;
		fibres.at((i+46)/2).init();
	}
	qDebug() << "========= init end";
}


void hairball::update()
{

	counter ++;
	if(counter == 360){
		counter = 0;
		direction = -direction;
	}
	for(int h = 0; h < fibres.size(); h++)
	{
		//float f_x = cosf((float)(counter)* PI / 180.0)*15*direction;
		//float f_y = cosf((float)((counter+45))* PI / 180.0)*9*(-direction);
		//float f_z = cosf((float)((counter+90))* PI / 180.0)*13*direction;

		float f_x = 0;
		float f_y = 0;
		float f_z = 0;//cos((float)(counter)* PI / 180.0)*15;

		// external forces
		// =============================================

		for(int i=4; i<fibres[h].nodes.size(); i++)
		{
			fibres[h].nodes[i].acc[0] = 0;
			fibres[h].nodes[i].acc[1] = g;
			fibres[h].nodes[i].acc[2] = 0;
		}
		// springs forces
		// =============================================
		for(int i=0; i<fibres[h].springs.size(); i++)
		{
			fibres[h].springs[i].rightNode->acc[0] += -k * fibres[h].springs[i].elong_x;
			fibres[h].springs[i].rightNode->acc[1] += -k * fibres[h].springs[i].elong_y;
			fibres[h].springs[i].rightNode->acc[2] += -k * fibres[h].springs[i].elong_z;

			fibres[h].springs[i].leftNode->acc[0] += k * fibres[h].springs[i].elong_x;
			fibres[h].springs[i].leftNode->acc[1] += k * fibres[h].springs[i].elong_y;
			fibres[h].springs[i].leftNode->acc[2] += k * fibres[h].springs[i].elong_z;
		}

		//qDebug() << fibres[0].springs[0].elong_x;

		for(int i=0; i<4; i++)
		{
			fibres[h].nodes[i].acc[0] = f_x;
			fibres[h].nodes[i].acc[1] = f_y;
			fibres[h].nodes[i].acc[2] = f_z;
		}

		// actual Position Update
		// ============================================
		for(int i=0; i<fibres[h].nodes.size(); i++)
		{
			// only update non static nodes
			//if (fibres[h].nodes[i].dynamic)
			{
				fibres[h].nodes[i].vel[0] += fibres[h].nodes[i].acc[0] *dt;
				fibres[h].nodes[i].vel[1] += fibres[h].nodes[i].acc[1] *dt;
				fibres[h].nodes[i].vel[2] += fibres[h].nodes[i].acc[2] *dt;


				fibres[h].nodes[i].pos[0] += fibres[h].nodes[i].vel[0] *dt;
				fibres[h].nodes[i].pos[1] += fibres[h].nodes[i].vel[1] *dt;
				fibres[h].nodes[i].pos[2] += fibres[h].nodes[i].vel[2] *dt;
			}
		}


		// spring Update
		// =============================================
		for(int i=0; i<fibres[h].springs.size(); i++)
		{
			fibres[h].springs.at(i).update();
		}
	}
}

void hairball::paint()
{
	//paintNodes();
	//paintSprings();
	paintHair();
}


void hairball::paintNodes()
{
	hair* h_it;
	float x,y,z;
	// for all hair fibres
	for(int h=0; h<fibres.size(); h++)
	{
		h_it = &fibres.at(h);
		// paint all hair nodes on fibre
		for(int n=0; n<h_it->nodes.size(); n++)
		{
			glColor3f(0.9f*(float(n%4)/3.0f), 0.9 ,0.9f*(float((n+2)%4)/3.0f));

			glPushMatrix();
				glTranslatef(h_it->nodes.at(n).pos.at(0),h_it->nodes.at(n).pos.at(1),h_it->nodes.at(n).pos.at(2));
				glutSolidSphere(0.2,10,8);
			glPopMatrix();
		}
	}
}

void hairball::paintSprings()
{
	hair* h_it = &fibres.at(0);
	HairSpring* s;
	for(int n=0; n<h_it->springs.size(); n++)
	{
		s =  &h_it->springs.at(n);

		float x0 = s->leftNode->pos.at(0);
		float y0 = s->leftNode->pos.at(1);
		float z0 = s->leftNode->pos.at(2);

		float x1 = s->rightNode->pos.at(0);
		float y1 = s->rightNode->pos.at(1);
		float z1 = s->rightNode->pos.at(2);

		glColor3f(1,0,0);
		glLineWidth(2.5);
		glBegin(GL_LINES);
			glVertex3f(x0, y0, z0);
			glVertex3f(x1, y1, z1);
		glEnd();

	}
}
void hairball::paintHair()
{
	hair* h_it;
	glColor3f(0.5,0.4,0.2);
	for(int h=0; h<fibres.size(); h++)
	{
		h_it = &fibres.at(h);

		hairNode *node1, *node2, *node3, *node4;
		for(int n=0; n<h_it->subdiv-1; n++)
		{
			node1 =  &h_it->nodes[n*4+0];
			node2 =  &h_it->nodes[n*4+1];
			node3 =  &h_it->nodes[n*4+2];
			node4 =  &h_it->nodes[n*4+3];
			float x0 = (node1->pos[0]+node2->pos[0]+node3->pos[0]+node4->pos[0])/4.0;
			float y0 = (node1->pos[1]+node2->pos[1]+node3->pos[1]+node4->pos[1])/4.0;
			float z0 = (node1->pos[2]+node2->pos[2]+node3->pos[2]+node4->pos[2])/4.0;

			node1 =  &h_it->nodes[n*4+4];
			node2 =  &h_it->nodes[n*4+5];
			node3 =  &h_it->nodes[n*4+6];
			node4 =  &h_it->nodes[n*4+7];
			float x1 = (node1->pos[0]+node2->pos[0]+node3->pos[0]+node4->pos[0])/4.0;
			float y1 = (node1->pos[1]+node2->pos[1]+node3->pos[1]+node4->pos[1])/4.0;
			float z1 = (node1->pos[2]+node2->pos[2]+node3->pos[2]+node4->pos[2])/4.0;


			glLineWidth(2.5);
			glBegin(GL_LINE_STRIP);
				glVertex3f(x0, y0, z0);
				glVertex3f(x1, y1, z1);
			glEnd();

		}

	}

}




void hairball::reset()
{
	fibres.clear();
	init();
	update();
}





void hairball::step(){

	update();
	emit callUpdate();
}



