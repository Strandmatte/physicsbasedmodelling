#include "ExportHGeo.h"

#include <QDebug>
#include <Eigen/Eigen>
#include <iostream>
#include <iomanip>
#include <fstream>


ExportHGeo::ExportHGeo(string name, vector<snowParticle> particlesIn, int frame)
{

	particles = particlesIn;
	// get framenumber with preceding zeros
	std::stringstream filename;
	filename << name << "_" << setfill('0') << setw(4) << frame << ".geo";
	string sName = filename.str();
	QString qName = QString::fromStdString(sName);
	qDebug() << "Writing geo file: " << qName;

	// get Point Count
	pointCount = particlesIn.size();

	// reset files
	fstream f;
	f.open(sName, std::ios_base::trunc | std::ios_base::out);
	f.close();

	// write data
	f.open(sName, std::ios_base::app | std::ios_base::out);

	f << header();
	f << attributes();
	f << footer();

	f.close();

}

ExportHGeo::~ExportHGeo(void)
{

}


// Return string containing all Positions
string ExportHGeo::pointPositions(){

	stringstream posStream;
	posStream << "[";
	for(int i=0; i<pointCount; i++){
		float x = particles[i].getPosition()[0];
		float y = particles[i].getPosition()[1];
		float z = particles[i].getPosition()[2];
		posStream << "[" << x << "," << y << "," << z << "," << "1" << "],";
	}
	// remove last komma and replace with brackets
	string positions = posStream.str();
	positions.replace(positions.length()-1,1,"]");

	//qDebug() << "Positions: " << QString::fromStdString(positions);

	return positions;
}


// Return string containing all Velocities
string ExportHGeo::pointVelocity(){

	stringstream velStream;
	velStream << "[";
	for(int i=0; i<pointCount; i++){
		float x = particles[i].getVelocity()[0];
		float y = particles[i].getVelocity()[1];
		float z = particles[i].getVelocity()[2];
		velStream << "[" << x << "," << y << "," << z << "],";
	}
	// remove last komma and replace with brackets
	string vels = velStream.str();
	vels.replace(vels.length()-1,1,"]");

	return vels;
}



// Return string containing header data
string ExportHGeo::header(){
	stringstream header;
	header << "[\n";
	header << "\t\"fileversion\",\"13.0.198.21\",\n";
	header << "\t\"pointcount\"," << to_string(pointCount) << ",\n";
	header << "\t\"vertexcount\",0,\n";
	header << "\t\"primitivecount\",0,\n";
	header << "\t\"info\",0,\n";

	/*  Nice to have:
	 "info",{
		"software":"Houdini 13.0.198.21",
		"hostname":"PC-Zimmer2",
		"artist":"Stefan",
		"timetocook":2.3e-005,
		"date":"2014-04-13 12:47:13",
		"time":3.83333333333,
		"bounds":[-1,1,0,2,-1,1],
		"attribute_summary":"     2 point attributes:\tvelocity, P\n"
	},
	*/

	header << "\t\"topology\",[\n";
	header << "\t\t\"pointref\",[\n";
	header << "\t\t\t\"indices\",[]\n";
	header << "\t\t]\n";
	header << "\t],\n";

	return header.str();
}

// Return string containing attributes
string ExportHGeo::attributes(){
	stringstream attributes;

	attributes << "\n\t\"attributes\",[";
	attributes << "\n\t\t\"vertexattributes\",[";
	attributes << "\n\t\t],";
	attributes << "\n\t\t\"pointattributes\",[";
	attributes << "\n\t\t\t[";
	attributes << "\n\t\t\t\t[";
	attributes << "\n\t\t\t\t\t\"scope\",\"public\",";
	attributes << "\n\t\t\t\t\t\"type\",\"numeric\",";
	attributes << "\n\t\t\t\t\t\"name\",\"P\",";
	attributes << "\n\t\t\t\t\t\"options\",{";
	attributes << "\n\t\t\t\t\t\t\"type\":{";
	attributes << "\n\t\t\t\t\t\t\t\"type\":\"string\",";
	attributes << "\n\t\t\t\t\t\t\t\"value\":\"hpoint\"";
	attributes << "\n\t\t\t\t\t\t}";
	attributes << "\n\t\t\t\t\t}";
	attributes << "\n\t\t\t\t],";
	attributes << "\n\t\t\t\t[";
	attributes << "\n\t\t\t\t\t\"size\",4,";
	attributes << "\n\t\t\t\t\t\"storage\",\"fpreal32\",";
	attributes << "\n\t\t\t\t\t\"defaults\",[";
	attributes << "\n\t\t\t\t\t\t\"size\",4,";
	attributes << "\n\t\t\t\t\t\t\"storage\",\"fpreal64\",";
	attributes << "\n\t\t\t\t\t\t\"values\",[0,0,0,1]";
	attributes << "\n\t\t\t\t\t],";
	attributes << "\n\t\t\t\t\t\"values\",[";
	attributes << "\n\t\t\t\t\t\t\"size\",4,";
	attributes << "\n\t\t\t\t\t\t\"storage\",\"fpreal32\",";
	attributes << "\n\t\t\t\t\t\t\"tuples\",";

	attributes << pointPositions();

	attributes << "\n\t\t\t\t\t]";
	attributes << "\n\t\t\t\t]";
	attributes << "\n\t\t\t],";

	attributes << "\n\t\t\t[";
	attributes << "\n\t\t\t\t[";
	attributes << "\n\t\t\t\t\t\"scope\",\"public\",";
	attributes << "\n\t\t\t\t\t\"type\",\"numeric\",";
	attributes << "\n\t\t\t\t\t\"name\",\"velocity\",";
	attributes << "\n\t\t\t\t\t\"options\",{";
	attributes << "\n\t\t\t\t\t}";
	attributes << "\n\t\t\t\t],";
	attributes << "\n\t\t\t\t[";
	attributes << "\n\t\t\t\t\t\"size\",3,";
	attributes << "\n\t\t\t\t\t\"storage\",\"fpreal32\",";
	attributes << "\n\t\t\t\t\t\"defaults\",[";
	attributes << "\n\t\t\t\t\t\t\"size\",3,";
	attributes << "\n\t\t\t\t\t\t\"storage\",\"fpreal64\",";
	attributes << "\n\t\t\t\t\t\t\"values\",[0,0,0]";
	attributes << "\n\t\t\t\t\t],";
	attributes << "\n\t\t\t\t\t\"values\",[";
	attributes << "\n\t\t\t\t\t\t\"size\",3,";
	attributes << "\n\t\t\t\t\t\t\"storage\",\"fpreal32\",";
	attributes << "\n\t\t\t\t\t\t\"tuples\",";

	attributes << pointVelocity();

	attributes << "\n\t\t\t\t\t]";
	attributes << "\n\t\t\t\t]";
	attributes << "\n\t\t\t]";
	attributes << "\n\t\t]";
	attributes << "\n\t]";

	return attributes.str();
}

// Return string containing footer data
string ExportHGeo::footer(){
	return ",\n \t\"primitives\",[\n \t] \n]";
}

