#include "tautochrone.h"
#include <math.h>
#include <QPainter>
#include <vector>
#include <QDebug>
#include "qtglut.h"
#include <fstream>

#define PI 3.14159265

//    CONSTRUCTOR
//#######################################################
Tautochrone::Tautochrone(){}
Tautochrone::Tautochrone(float pos)
{
	qDebug() << "Tautochrone Erstellt";
	particles.push_back(0);

	init();

	particles[0].position = startPos = pos;
	particles[0].tang_vel = 0;
	particles[0].acceleration = g;
	particles[0].mass = 0.1;
}

Tautochrone::~Tautochrone()
{
	//qDebug() << "Tautochrone gelöscht";
}


//    Initialize
//#######################################################
void Tautochrone::init()
{
	g = 9.81;
	t = 0.0f;
	dt = 0.004;

	float res = 0.1f;
	float x = 0.0f;
	while (x<2*PI)
	{
		curve_tauto.push_back(curve_function(x));
		//qDebug() << curve_function(x).x() << " | " <<  curve_function(x).y();
		x+= res;
	}

}


//    Update
//#######################################################
void Tautochrone::update()
{
	if (t<PI){
		step();
		dataOut();
		t+=dt;
	}
}
//    Step
//#######################################################
void Tautochrone::step()
{
	// move particles
	moveParticle(&particles[0]);
}

//    Function describing the Tautochrone
//#######################################################
QPointF Tautochrone::curve_function(float phi)
{
	return QPointF(phi-sin(phi),cos(phi)-1);
}


//    update Particle position per step
//#######################################################
void Tautochrone::moveParticle(Particle *p)
{
	qDebug() << "velocity: " << p->velocity.x();
	qDebug() << "position: " << p->position;

	// acceleration
	if(p->position > 0)	p->acceleration = ((g*sin(p->position))-(p->tang_vel*sin(p->position)))/(2-(2*cos(p->position)));

	// calculate new velocity
  	p->tang_vel = p->tang_vel + dt*p->acceleration;

	// calculate new position
	p->position = p->position + dt*p->tang_vel;
	if(p->position > 2*PI) p->position = p->position - 2*PI;

	// save data in curves
	p->curve_vel.append( QPointF(t,p->velocity.x()) ); // Todo: Velocity in tangentialrichtung
	p->curve_pos.append( QPointF(t,p->position) );

	emit callUpdate();

}


//    write Poition and Speed in File
//#######################################################
void Tautochrone::dataOut()
{
	fstream f;
	f.open("position", std::ios_base::app | std::ios_base::out);
	f << t << " | " <<  particles.at(0).position << " | " << particles.at(1).position << " | " << particles.at(2).position << endl;
	f.close();

	f.open("speed", std::ios_base::app | std::ios_base::out);
	f << t << " | " << particles.at(0).tang_vel << " | " << particles.at(1).tang_vel << " | " << particles.at(2).tang_vel << endl;
	f.close();

	//qDebug() << "Data written";

}

void Tautochrone::paint(){

	glColor3f(1.0,0,0);

	glBegin(GL_QUADS);
	for(int i = 0; i < curve_tauto.size()-1; i++){
		glVertex3f(curve_tauto[i].x(),curve_tauto[i].y()+2,0);
		glVertex3f(curve_tauto[i+1].x(),curve_tauto[i+1].y()+2,0);
		glVertex3f(curve_tauto[i+1].x(),curve_tauto[i+1].y()+2,0.5);
		glVertex3f(curve_tauto[i].x(),curve_tauto[i].y()+2,0.5);
	}
	glEnd();

	QPointF tmpPos = curve_function(particles[0].position);

	glColor3f(0,1,0);
	glTranslatef(tmpPos.x(),tmpPos.y()+2.1,0.25);
	glutSolidSphere(particles[0].mass,10,8);
	glTranslatef(-tmpPos.x(),-(tmpPos.y()+2.1),-0.25);

}

void Tautochrone::reset(){
	if(startPos == 0) startPos = 0.1;
	particles[0].position = startPos;
	particles[0].tang_vel = 0;
	particles[0].acceleration = g;
}
