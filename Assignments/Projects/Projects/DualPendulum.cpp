#include "DualPendulum.h"
#include "qtglut.h"
#include <math.h>

dualPendulum::dualPendulum(){

	g = 9.81;
	dt = 0.008;
}

dualPendulum::~dualPendulum(){}

void dualPendulum::setAbsPos(){
	m1.global_position.setX(l1*sin(m1.position));
	m1.global_position.setY(-l1*cos(m1.position));
	m2.global_position.setX(l1*sin(m1.position)+(l2*sin(m2.position)));
	m2.global_position.setY(-l1*cos(m1.position)-(l2*cos(m2.position)));
}

void dualPendulum::paint(){

	glColor3f(0,0,0);
	glBegin(GL_LINES);
		glVertex3f(0,0,0);
		glVertex3f(m1.global_position.x(),m1.global_position.y(),0);
		
		glVertex3f(m1.global_position.x(),m1.global_position.y(),0);
		glVertex3f(m2.global_position.x(),m2.global_position.y(),0);
	glEnd();

	glColor3f(0,1,0);
	glTranslatef(m1.global_position.x(),m1.global_position.y(),0);
	glutSolidSphere(m1.mass,10,8);
	glTranslatef(-m1.global_position.x(),-m1.global_position.y(),0);
		
	glTranslatef(m2.global_position.x(),m2.global_position.y(),0);
	glutSolidSphere(m2.mass,10,8);
	glTranslatef(-m2.global_position.x(),-m2.global_position.y(),0);

}

void dualPendulum::reset(){
	m1.tang_vel = 0;
	m1.acceleration = g;
	m2.tang_vel = 0;
	m2.acceleration = g;
	setAbsPos();
}

void dualPendulum::step(){

	m1.acceleration = -(m2.mass*l2*m2.acceleration*cos(m1.position-m2.position)+m2.mass*l2*pow(m2.tang_vel,2)*sin(m1.position-m2.position)+(m1.mass+m2.mass)*g*sin(m1.position))/((m1.mass+m2.mass)*l1);
	m2.acceleration = -(l1*m1.acceleration*cos(m1.position-m2.position)-l1*pow(m1.tang_vel,2)*sin(m1.position-m2.position)+g*sin(m2.position))/l2;

	m1.tang_vel += m1.acceleration * dt;
	m2.tang_vel += m2.acceleration * dt;

	m1.position += m1.tang_vel * dt;
	m2.position += m2.tang_vel * dt;

	setAbsPos();

	emit callUpdate();
}