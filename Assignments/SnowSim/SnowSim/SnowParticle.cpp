#include "snowParticle.h"

/*
* snowParticle defines a single Snow-Particle
* with position, velocity, mass, volume and density
*/

// CONSTRUCTORS
snowParticle::snowParticle(){
	this->position = Eigen::Vector3f(0.0f,0.0f,0.0f);
	this->velocity = Eigen::Vector3f(0.0f,0.0f,0.0f);
	this->mass = 1.0;
}
snowParticle::snowParticle(Eigen::Vector3f pos, Eigen::Vector3f vel){
	this->position = pos;
	this->velocity = vel;
	this->mass = 1.0;
}
snowParticle::snowParticle(float posX, float posY, float posZ, float velX, float velY, float velZ){
	this->position = Eigen::Vector3f(posX,posY,posZ);
	this->velocity = Eigen::Vector3f(velX,velY,velZ);
	this->mass = 1.0;
}
snowParticle::~snowParticle(){

}


// SETTER
void snowParticle::setPosition(float posX, float posY, float posZ){
	this->position = Eigen::Vector3f(posX,posY,posZ);
}
void snowParticle::setVelocity(float velX, float velY, float velZ){
	this->velocity = Eigen::Vector3f(velX, velY, velZ);
}
void snowParticle::setMass(float mass){
	this->mass = mass;
}
void snowParticle::setVolume(float volume){
	this->volume = volume;
}
void snowParticle::setDensity(float density){
	this->density = density;
}

// GETTER
Eigen::Vector3f snowParticle::getPosition(){
	return this->position;
}
Eigen::Vector3f snowParticle::getVelocity(){
	return this->velocity;
}
float snowParticle::getMass(){
	return this->mass;
}
float snowParticle::getVolume(){
	return this->volume;
}
float snowParticle::getDensity(){
	return this->density;
}
