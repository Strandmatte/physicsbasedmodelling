#pragma once

#include <vector>

using namespace std;

class hairNode
{
public:
	hairNode(void);
	~hairNode(void);

	vector<float>			 acc;
	vector<float>            vel;
	vector<float>            pos;
	float                    mass;
	bool                     dynamic;
};

