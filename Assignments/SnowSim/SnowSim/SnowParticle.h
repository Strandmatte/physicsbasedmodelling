#ifndef SNOWPARTICEL_H
#define SNOWPARTICEL_H

#include <Eigen/Eigen>

class snowParticle{

public:
	snowParticle();
	snowParticle(float posX, float posY, float posZ, float velX, float velY, float velZ);
	snowParticle(Eigen::Vector3f pos, Eigen::Vector3f vel);
	~snowParticle();

	// Getter Setter
	void                       setPosition(float posX, float posY, float posZ);
	void                       setVelocity(float velX, float velY, float velZ);
	void                       setMass(float mass);
	void                       setVolume(float volume);
	void                       setDensity(float density);

	Eigen::Vector3f            getPosition();
	Eigen::Vector3f            getVelocity();
	float                      getMass();
	float                      getVolume();
	float                      getDensity();

private:
	Eigen::Vector3f            position;
	Eigen::Vector3f            velocity;
	float                      mass;
	float                      volume;
	float                      density;
	// deformation Gradient Fp
};

#endif // SNOWPARTICEL_H

