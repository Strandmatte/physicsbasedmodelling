#ifndef SNOWSIM_H
#define SNOWSIM_H

#include <Eigen/Eigen>
#include <qwidget.h>
#include "qtglut.h"
#include "snowSolver.h"
#include "snowObject.h"
#include "solidObject.h"
#include <memory>

using namespace std;

class snowSim : public QObject{
	Q_OBJECT

public:
	snowSim();
	~snowSim();

	void                paint();
	void                paintParticles();
	void                paintGrid();
	void                paintVel();
	void                paintMass();

	float               E0;
	float               thetaC;
	float               thetaS;
	float               xi;
	float               rho0;
	float               nu;

	snowSolver*         solver;

	void reset();

	public slots:
		void            step();
		void			switchSim(int i);

protected:
	double              timeStep;
	double              time;
	int                 frame;
	Eigen::Vector3i		subdiv;
	Eigen::Vector3f		scale;
	Eigen::Vector3f		csize;
	Eigen::Vector3f		pos;
	snowObject*			p;
};

#endif // SNOWSIM_H
