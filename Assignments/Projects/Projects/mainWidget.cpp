#include "mainWidget.h"

mainWidget::mainWidget(QWidget *parent) : QWidget(parent){

	createGridGroupBox();

	setLayout(layout);
	setWindowTitle("Physics Based Modelling Projekte");
}

mainWidget::~mainWidget(){
	glWindow->~OGLWidget();
	label->~QLabel();
	startBtn->~QPushButton();
	stopBtn->~QPushButton();
}

void mainWidget::createGridGroupBox(){

	
	layout = new QGridLayout();
	layout->setColumnStretch(0,8);
	layout->setColumnStretch(1,1);
	layout->setColumnStretch(2,1);
	
	glWindow = new OGLWidget(this);

	startBtn = new QPushButton();
	startBtn->setText("start");

	stopBtn = new QPushButton();
	stopBtn->setText("stop");

	resetBtn = new QPushButton();
	resetBtn->setText("reset");

	label = new QLabel(this);

	label->setWordWrap(true);

	label->setText(QString("R: reset Camera"));

	projectLabel = new QLabel();
	projectLabel->setText(QString::fromLatin1("Project:"));

	projectChoice = new QComboBox();
	projectChoice->addItems(QStringList() << "Hairball" << "Foucault-Pendulum" << "Coupled Pendulum" << "Dual Pendulum" << "Tautochrone");
	
	comboPropLabel = new QLabel();
	comboProp = new QComboBox();

	prop1Label = new QLabel();
	prop1 = new QLineEdit();
	prop2Label = new QLabel();
	prop2 = new QLineEdit();
	prop3Label = new QLabel();
	prop3 = new QLineEdit();
	prop4Label = new QLabel();
	prop4 = new QLineEdit();
	prop5Label = new QLabel();
	prop5 = new QLineEdit();
	prop6Label = new QLabel();
	prop6 = new QLineEdit();
	prop7Label = new QLabel();
	prop7 = new QLineEdit();
	prop8Label = new QLabel();
	prop8 = new QLineEdit();
	prop9Label = new QLabel();
	prop9 = new QLineEdit();
	prop10Label = new QLabel();
	prop10 = new QLineEdit();

	layout->addWidget(glWindow,0,0,-1,1);

	layout->addWidget(projectLabel,1,1,1,1,Qt::AlignRight);
	layout->addWidget(projectChoice,1,2,1,1,Qt::AlignLeft);	
	layout->addWidget(comboPropLabel,2,1,1,1,Qt::AlignRight);
	layout->addWidget(comboProp,2,2,1,1,Qt::AlignLeft);
	layout->addWidget(prop1Label,3,1,1,1,Qt::AlignRight);
	layout->addWidget(prop1,3,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop2Label,4,1,1,1,Qt::AlignRight);
	layout->addWidget(prop2,4,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop3Label,5,1,1,1,Qt::AlignRight);
	layout->addWidget(prop3,5,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop4Label,6,1,1,1,Qt::AlignRight);
	layout->addWidget(prop4,6,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop5Label,7,1,1,1,Qt::AlignRight);
	layout->addWidget(prop5,7,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop6Label,8,1,1,1,Qt::AlignRight);
	layout->addWidget(prop6,8,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop7Label,9,1,1,1,Qt::AlignRight);
	layout->addWidget(prop7,9,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop8Label,10,1,1,1,Qt::AlignRight);
	layout->addWidget(prop8,10,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop9Label,11,1,1,1,Qt::AlignRight);
	layout->addWidget(prop9,11,2,1,1,Qt::AlignLeft);	
	layout->addWidget(prop10Label,12,1,1,1,Qt::AlignRight);
	layout->addWidget(prop10,12,2,1,1,Qt::AlignLeft);	
	layout->addWidget(startBtn,13,1,1,2,Qt::AlignCenter);	
	layout->addWidget(stopBtn,14,1,1,2,Qt::AlignCenter);	
	layout->addWidget(resetBtn,15,1,1,2,Qt::AlignCenter);
	layout->addWidget(label,16,1,1,2,Qt::AlignCenter);

	connect(projectChoice,SIGNAL(currentIndexChanged(int)),this,SLOT(switchProject(int)));
	connect(startBtn,SIGNAL(clicked()),this,SLOT(start()));
	connect(stopBtn,SIGNAL(clicked()),this,SLOT(stop()));
	connect(resetBtn,SIGNAL(clicked()),this,SLOT(reset()));

	switchProject(0);

	glWindow->setFocus();
}

void mainWidget::mousePressEvent(QMouseEvent *event){
	glWindow->mousePressEvent(event);
}

void mainWidget::mouseReleaseEvent(QMouseEvent *event){
	glWindow->mouseReleaseEvent(event);
}

void mainWidget::mouseMoveEvent(QMouseEvent *event){

}

void mainWidget::wheelEvent(QWheelEvent *event){
	glWindow->wheelEvent(event);
}

void mainWidget::keyPressEvent(QKeyEvent *event){

	switch(event->key()) {
	case Qt::Key_Up:
	case Qt::Key_Down:
	break;
	default:
	break;
	}
}

void mainWidget::switchProject(int index){
	glWindow->timer.disconnect();

	prop1Label->hide();
	prop2Label->hide();
	prop3Label->hide();
	prop4Label->hide();
	prop5Label->hide();
	prop6Label->hide();
	prop7Label->hide();
	prop8Label->hide();
	prop9Label->hide();
	prop10Label->hide();
	comboPropLabel->hide();
	comboProp->hide();
	prop1->hide();
	prop2->hide();
	prop3->hide();
	prop4->hide();
	prop5->hide();
	prop6->hide();
	prop7->hide();
	prop8->hide();
	prop9->hide();
	prop10->hide();

	switch(index){
	case 0:
		prop1Label->setText("Fiber-Length");
		prop2Label->setText("timestepp");
		prop3Label->setText("timer-Intervall(ms)");
		prop1Label->show();
		prop2Label->show();
		prop3Label->show();
		prop1->setText("5");
		prop2->setText("0.005");
		prop3->setText("2");
		prop1->show();
		prop2->show();
		prop3->show();
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),&glWindow->hb,SLOT(step()));
		break;
	case 1:
		comboPropLabel->setText(QString::fromLatin1("Integration-Method:"));
		comboProp->addItems(QStringList() << "Euler" << "Backward Euler" << "Runge-Kutta" << "Verlet");
		comboProp->setCurrentIndex(0);
		prop1Label->setText("Rod-Length");
		prop2Label->setText("Day-Length");
		prop3Label->setText("Latitude");
		prop4Label->setText("Mass");
		prop5Label->setText("x1");
		prop6Label->setText("x2");
		prop7Label->setText("v1");
		prop8Label->setText("v2");
		prop9Label->setText("timestep");
		prop10Label->setText("timer-intervall (ms)");
		prop1->setText("5");
		prop2->setText("30");
		prop3->setText("51.5167");
		prop4->setText("0.2");
		prop5->setText("1");
		prop6->setText("1");
		prop7->setText("0");
		prop8->setText("0");
		prop9->setText("0.005");
		prop10->setText("2");
		prop1Label->show();
		prop2Label->show();
		prop3Label->show();
		prop4Label->show();
		prop5Label->show();
		prop6Label->show();
		prop7Label->show();
		prop8Label->show();
		prop9Label->show();
		prop10Label->show();
		comboPropLabel->show();
		comboProp->show();
		prop1->show();
		prop2->show();
		prop3->show();
		prop4->show();
		prop5->show();
		prop6->show();
		prop7->show();
		prop8->show();
		prop9->show();
		prop10->show();
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),&glWindow->fp,SLOT(step()));
		break;
	case 2:
		prop1Label->setText("Start Phi1");
		prop2Label->setText("Length1");
		prop3Label->setText("Mass1");
		prop4Label->setText("Start Phi2");
		prop5Label->setText("Length2");
		prop6Label->setText("Mass2");
		prop7Label->setText("Spring-Length");
		prop8Label->setText("Spring-Constant");
		prop1->setText("-0.2");
		prop2->setText("5");
		prop3->setText("1");
		prop4->setText("0.5");
		prop5->setText("5");
		prop6->setText("1");
		prop7->setText("4");
		prop8->setText("1.5");
		prop1Label->show();
		prop2Label->show();
		prop3Label->show();
		prop4Label->show();
		prop5Label->show();
		prop6Label->show();
		prop7Label->show();
		prop8Label->show();
		prop1->show();
		prop2->show();
		prop3->show();
		prop4->show();
		prop5->show();
		prop6->show();
		prop7->show();
		prop8->show();
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),&glWindow->cp,SLOT(step()));
		break;
	case 3:
		prop1Label->setText("Start Phi1");
		prop2Label->setText("Length1");
		prop3Label->setText("Mass1");
		prop4Label->setText("Start Phi2");
		prop5Label->setText("Length2");
		prop6Label->setText("Mass2");
		prop1->setText("1.2");
		prop2->setText("5");
		prop3->setText("1");
		prop4->setText("-0.5");
		prop5->setText("5");
		prop6->setText("1");
		prop1Label->show();
		prop2Label->show();
		prop3Label->show();
		prop4Label->show();
		prop5Label->show();
		prop6Label->show();
		prop1->show();
		prop2->show();
		prop3->show();
		prop4->show();
		prop5->show();
		prop6->show();
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),&glWindow->dp,SLOT(step()));
		break;
	case 4:
		prop1Label->setText("StartPosition1");
		prop2Label->setText("StartPosition2");
		prop3Label->setText("StartPosition3");
		prop1->setText("0");
		prop2->setText("1");
		prop3->setText("2");
		prop1Label->show();
		prop2Label->show();
		prop3Label->show();
		prop1->show();
		prop2->show();
		prop3->show();
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),glWindow->t1,SLOT(step()));
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),glWindow->t2,SLOT(step()));
		QObject::connect(&glWindow->timer,SIGNAL(timeout()),glWindow->t3,SLOT(step()));
		break;
	default:
		break;
	}
	stop();
	reset();
	glWindow->p = index;
	glWindow->makeUpdate();
}

void mainWidget::start(){
	switch(projectChoice->currentIndex()){
	case 0:
		glWindow->timer.start(prop3->text().toInt());
		break;
	case 1:
		glWindow->timer.start(prop10->text().toInt());
		break;
	default:
		glWindow->timer.start(5);
		break;
	}
	reset();
	glWindow->setFocus();
}

void mainWidget::stop(){
	glWindow->timer.stop();
	glWindow->setFocus();
}

void mainWidget::reset(){
	switch(projectChoice->currentIndex()){
	case 0:
		glWindow->hb.l = prop1->text().toFloat();
		glWindow->hb.dt = prop2->text().toFloat();
		glWindow->hb.reset();
		break;
	case 1:
		glWindow->fp.m.mass = prop4->text().toFloat();
		glWindow->fp.l = prop1->text().toFloat()+glWindow->fp.m.mass/2;
		glWindow->fp.T = prop2->text().toFloat();
		glWindow->fp.lat = prop3->text().toFloat();		
		glWindow->fp.m.global_position.setX(prop5->text().toFloat());
		glWindow->fp.m.globalZ = prop6->text().toFloat();
		glWindow->fp.m.tang_vel = prop7->text().toFloat();
		glWindow->fp.m.tang_velZ = prop8->text().toFloat();
		glWindow->fp.dt = prop9->text().toFloat();
		glWindow->fp.integrator = comboProp->currentIndex();
		glWindow->fp.reset();
		break;
	case 2:
		glWindow->cp.m1.position = prop1->text().toFloat();
		glWindow->cp.m1.mass = prop3->text().toFloat();
		glWindow->cp.l1 = prop2->text().toFloat()+glWindow->cp.m1.mass/2;
		glWindow->cp.m2.position = prop4->text().toFloat();
		glWindow->cp.m2.mass = prop6->text().toFloat();
		glWindow->cp.l2 = prop5->text().toFloat()+glWindow->cp.m2.mass/2;
		glWindow->cp.m2.mass = prop6->text().toFloat();
		glWindow->cp.sl = prop7->text().toFloat();
		glWindow->cp.k = prop8->text().toFloat();
		glWindow->cp.reset();
		break;
	case 3:
		glWindow->dp.m1.position = prop1->text().toFloat();
		glWindow->dp.m1.mass = prop3->text().toFloat();
		glWindow->dp.l1 = prop2->text().toFloat()+glWindow->dp.m1.mass/2;
		glWindow->dp.m2.position = prop4->text().toFloat();
		glWindow->dp.m2.mass = prop6->text().toFloat();
		glWindow->dp.l2 = prop5->text().toFloat()+glWindow->dp.m1.mass/2+glWindow->dp.m2.mass/2;
		glWindow->dp.reset();
		break;
	case 4:
		glWindow->t1->startPos = prop1->text().toFloat();
		glWindow->t2->startPos = prop2->text().toFloat();
		glWindow->t3->startPos = prop3->text().toFloat();
		glWindow->t1->reset();
		glWindow->t2->reset();
		glWindow->t3->reset();
		break;
	default:
		break;
	}
	glWindow->makeUpdate();
	glWindow->setFocus();
}