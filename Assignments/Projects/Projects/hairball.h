#pragma once
#ifndef HAIRBALL_H
#define HAIRBALL_H

#include <QWidget>
#include <vector>
#include <QtOpenGL/QGLWidget>
#include "hair.h"
#include "qtglut.h"
#include <math.h>
#include "hairspring.h"


using namespace std;

class hairball : public QObject{

	Q_OBJECT

public:
	hairball();
	~hairball();

	float                              dt;
	float                              g;
	float                              l;
	float                              k;  // spring constant
	
	vector<hair>                       fibres;

	void                               init();
	void                               update();
	void                               paint();
	void                               paintSprings();
	void                               paintNodes();
	void                               paintHair();

public slots:
	void                               reset();
	void                               step();

signals:
		void callUpdate();

protected:
	int counter;
	int direction;
};

#endif //HAIRBALL_H
