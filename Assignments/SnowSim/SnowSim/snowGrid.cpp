#include "snowGrid.h"
#include <QDebug>

snowGrid::snowGrid(float i, float j, float k, int x, int y, int z)
{
	// cells per dimesion
	subdiv << x,y,z;

	cellsize << i,j,k;

	qDebug() << "subdiv" <<subdiv(0) << subdiv(1) << subdiv(2);
	qDebug() << "cellsize" << cellsize(0) << cellsize(1) << cellsize(2);

	// fill grid with arbitrary numbers
	for(int i=0; i<IX(x,y,z); i++){
		masses.push_back(float(i));

		Eigen::Vector3f v;
		velocities.push_back(v);

		Eigen::Vector3f f;
		forces.push_back(f);
	}

	
}
snowGrid::~snowGrid(){
	masses.clear();
	velocities.clear();
	forces.clear();
}

Eigen::Vector3i snowGrid::getSubdiv(){
	return subdiv;
}
Eigen::Vector3f snowGrid::getCellsize(){
	return cellsize;
}

int snowGrid::IX(int i,int j,int k){
	return	((i)+(subdiv(0))*(j)+(subdiv(0)*subdiv(1))*(k));
}
