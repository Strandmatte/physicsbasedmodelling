#pragma once
#ifndef SNOWSOLVER_H
#define SNOWSOLVER_H

#include "snowObject.h"
#include <Eigen/Eigen>
#include "solidObject.h"
#include "snowGrid.h"
#include <memory>

using namespace std;

class snowSolver{

public:
	snowSolver(vector<snowObject*> obj);
	~snowSolver();

	snowObject*                     getSnowObject(int pos);
	snowGrid*						grid;
	Eigen::Vector3f					minPos;
	void                            init();
	void                            update(double dt);
	void							setDensities(float rho);
	void							refresh(vector<snowObject*> obj);

protected:
	vector<snowObject*>             snowObjects;
	void                            rasterize();
	float                           N(float x);

};

#endif //SNOWSOLVER_H
