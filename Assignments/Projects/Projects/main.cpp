#include <QtWidgets/QApplication>
#include <QtOpenGL/QGLWidget>
#include "mainWidget.h"

int main(int argc, char *argv[]) {

	QApplication app(argc, argv);

	mainWidget window;

	window.showMaximized();


	return app.exec();
}
