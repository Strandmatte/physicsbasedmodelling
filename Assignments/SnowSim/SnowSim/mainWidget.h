#pragma once

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtOpenGL/QGLWidget>
#include <QtGui>
#include <QtWidgets>
#include <QPushButton>
#include <QtWebKit>
#include <QtWebKitWidgets>
#include "OGLWidget.h"
#include <math.h>

#define USE_MATH_DEFINES

class mainWidget : public QWidget {

    Q_OBJECT // must include this if you use Qt signals/slots

public:

    mainWidget(QWidget *parent = NULL);
	~mainWidget(void);

	/*void calculatingProg(bool showCalcBtn);*/

	public slots:

protected:
    
	OGLWidget* glWindow;

	QLabel* label;
	QPushButton* startBtn;
	QPushButton* stopBtn;
	QPushButton* resetBtn;
	QPushButton* paperBtn;
	QPushButton* specsBtn;
	QLabel* comboPropLabel;
	QComboBox* comboProp;
	QLabel* prop1Label;
	QLineEdit* prop1;
	QLabel* prop2Label;
	QLineEdit* prop2;
	QLabel* prop3Label;
	QLineEdit* prop3;
	QLabel* prop4Label;
	QLineEdit* prop4;
	QLabel* prop5Label;
	QLineEdit* prop5;
	QLabel* prop6Label;
	QLineEdit* prop6;
	QLabel* prop7Label;
	QLineEdit* prop7;

	QWebView* vid;

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);

	protected slots:
		void start();
		void stop();
		void reset();
		void openPaper();
		void openSpecs();
		void switchSim(int i);

private:
	QGridLayout* layout;

	void createGridGroupBox();
	void setVideoHtml();
};

#endif//MAINWIDGET_H
