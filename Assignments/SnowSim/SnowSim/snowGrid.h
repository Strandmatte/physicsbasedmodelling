#ifndef SNOWGRID_H
#define SNOWGRID_H

#include <Eigen/Eigen>
#include <math.h>
#include <vector>

using namespace std;


class snowGrid
{
public:
	snowGrid(float i, float j, float k, int x, int y, int z);
	~snowGrid();

	Eigen::Vector3i  getSubdiv();
	Eigen::Vector3f  getCellsize();


	vector<float> masses;
	vector<Eigen::Vector3f> forces;
	vector<Eigen::Vector3f> velocities;

	Eigen::Vector3f  cellsize;
	Eigen::Vector3i  subdiv; // cells per dimension

	int              IX(int i, int j,int k);

};

#endif // SNOWGRID_H
