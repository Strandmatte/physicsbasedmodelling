#pragma once
#ifndef DOUBLEPENDULU_H
#define DOUBLEPENDULU_H

#include <QWidget>
#include <vector>
#include <QtOpenGL/QGLWidget>
#include "particle.h"

using namespace std;

class dualPendulum : public QObject{

	Q_OBJECT

public:
	dualPendulum();
	~dualPendulum();

	void setAbsPos();
	void paint();

	float dt;
	float g;
	float l1;
	float l2;

	Particle m1;
	Particle m2;

	public slots:
		void reset();
		void step();

signals:
		void callUpdate();
};

#endif //DOUBLEPENDULU_H