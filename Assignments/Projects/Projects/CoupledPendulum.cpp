#include "CoupledPendulum.h"
#include "qtglut.h"
#include <math.h>

coupledPendulum::coupledPendulum(){

	g = 9.81;
	dt = 0.008;
}

coupledPendulum::~coupledPendulum(){}

void coupledPendulum::setAbsPos(){
	m1.global_position.setX(l1*sin(m1.position));
	m1.global_position.setY(-l1*cos(m1.position));
	m2.global_position.setX(l2*sin(m2.position));
	m2.global_position.setY(-l2*cos(m2.position));
}

void coupledPendulum::paint(){

	glColor3f(0,0,0);
	glBegin(GL_LINES);
		glVertex3f(-sl/2,0,0);
		glVertex3f(m1.global_position.x()-sl/2,m1.global_position.y(),0);
		
		glVertex3f(sl/2,0,0);
		glVertex3f(m2.global_position.x()+sl/2,m2.global_position.y(),0);

		glColor3f(1,0,0);
		glVertex3f(m1.global_position.x()-sl/2,m1.global_position.y(),0);
		glVertex3f(m2.global_position.x()+sl/2,m2.global_position.y(),0);
	glEnd();

	glColor3f(0,1,0);
	glTranslatef(m1.global_position.x()-sl/2,m1.global_position.y(),0);
	glutSolidSphere(m1.mass,10,8);
	glTranslatef(-(m1.global_position.x()-sl/2),-m1.global_position.y(),0);
		
	glTranslatef(m2.global_position.x()+sl/2,m2.global_position.y(),0);
	glutSolidSphere(m2.mass,10,8);
	glTranslatef(-(m2.global_position.x()+sl/2),-m2.global_position.y(),0);

}

void coupledPendulum::reset(){
	m1.tang_vel = 0;
	m1.acceleration = g;
	m2.tang_vel = 0;
	m2.acceleration = g;

	sl += sin(m1.position)*l1 + sin(m2.position)*l2;

	setAbsPos();
}

void coupledPendulum::step(){

	m1.acceleration = (-1/(m1.mass*l1))*(m1.mass*g*m1.position+k*(l1*m1.position-l2*m2.position));
	m2.acceleration = (-1/(m2.mass*l2))*(m2.mass*g*m2.position+k*(l2*m2.position-l1*m1.position));

	m1.tang_vel += m1.acceleration * dt;
	m2.tang_vel += m2.acceleration * dt;

	m1.position += m1.tang_vel * dt;
	m2.position += m2.tang_vel * dt;

	setAbsPos();

	emit callUpdate();
}