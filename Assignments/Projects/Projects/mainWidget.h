#pragma once

#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtOpenGL/QGLWidget>
#include <QtGui>
#include <QtWidgets>
#include <QPushButton>
#include "OGLWidget.h"

class mainWidget : public QWidget {

    Q_OBJECT // must include this if you use Qt signals/slots

public:

    mainWidget(QWidget *parent = NULL);
	~mainWidget(void);

	/*void calculatingProg(bool showCalcBtn);*/

	public slots:

protected:
    
	OGLWidget* glWindow;

	QLabel* label;
	QPushButton* startBtn;
	QPushButton* stopBtn;
	QPushButton* resetBtn;
	QLabel* projectLabel;
	QComboBox* projectChoice;
	QLabel* comboPropLabel;
	QComboBox* comboProp;
	QLabel* prop1Label;
	QLineEdit* prop1;
	QLabel* prop2Label;
	QLineEdit* prop2;
	QLabel* prop3Label;
	QLineEdit* prop3;
	QLabel* prop4Label;
	QLineEdit* prop4;
	QLabel* prop5Label;
	QLineEdit* prop5;
	QLabel* prop6Label;
	QLineEdit* prop6;
	QLabel* prop7Label;
	QLineEdit* prop7;
	QLabel* prop8Label;
	QLineEdit* prop8;
	QLabel* prop9Label;
	QLineEdit* prop9;
	QLabel* prop10Label;
	QLineEdit* prop10;


	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);

	protected slots:
		void switchProject(int index);
		void start();
		void stop();
		void reset();

private:
	QGridLayout* layout;

	void createGridGroupBox();
};

#endif//MAINWIDGET_H