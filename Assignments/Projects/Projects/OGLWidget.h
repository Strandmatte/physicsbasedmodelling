#pragma once
#ifndef OGLWIDGET_H
#define OGLWIDGET_H

#include <QtOpenGL/QGLWidget>
#include <QtGui>
#include <vector>
#include "tautochrone.h"
#include "DualPendulum.h"
#include "CoupledPendulum.h"
#include "FoucaultPendulum.h"
#include "hairball.h"

class OGLWidget : public QGLWidget{

    Q_OBJECT // must include this if you use Qt signals/slots

public:

	float eye[3];
	float at[3];
	float up[3];

	float trans[3];
	float rot[2];

	float znear;
	float zfar;
	float WperH;

	int mpos[2];

	bool mPressedL;
	bool mPressedM;
	bool mPressedR;

	bool animating;
	
	int p;

	QTimer timer;

    OGLWidget(QWidget *parent = NULL);
	~OGLWidget(void);
	
	Tautochrone* t1;
	Tautochrone* t2;
	Tautochrone* t3;

	dualPendulum dp;

	coupledPendulum cp;

	foucaultPendulum fp;

	hairball hb;

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

	void mousePressEvent(QMouseEvent *event);
	void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
	void wheelEvent(QWheelEvent *event);
    void keyPressEvent(QKeyEvent *event);

	public slots:
		void makeUpdate();
	signals:

protected:

};

#endif // OGLWIDGET_H
