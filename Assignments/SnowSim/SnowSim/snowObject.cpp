#include "snowObject.h"
#include <math.h>
#include <iostream>
#include <qdebug.h>
/*
* snow Object defines a the scene-setup
* containing snow-shape, forces and collision objects
*/

snowObject::snowObject(int t){
	switch(t){
	case 0:
		snowball2D();
		break;
	case 1:
		snowball3D();
		break;
	default:
		break;
	}
}

snowObject::~snowObject(){

}

// Create 2D snowball
void snowObject::snowball2D(){

	particles.clear();

	x = 0.50f;

	int radius = 8;
	snowParticle particle;

	for(int i=-radius; i<=radius; i++){
		for(int j=-radius; j<=radius; j++){
			if((i*i+j*j)+(i*i+j*j) <= radius*radius){
				particle.setPosition(i,j,0);
				particle.setMass(0.5);
				particles.push_back(particle);
            }
		}
	}
}

// Create 3D snowball
void snowObject::snowball3D(){

	particles.clear();

	x = 0.50f;

	int radius = 5;
	snowParticle particle;

	for(float i=-radius; i<=radius; i+=0.3){
		for(float j=-radius; j<=radius; j+=0.3){
			for(float k=-radius; k<=radius; k+=0.3){
				if(((i*i+j*j)+(i*i+j*j) <= radius*radius) && ((i*i+k*k)+(i*i+k*k) <= radius*radius) && ((k*k+j*j)+(k*k+j*j) <= radius*radius)){
					particle.setPosition(i,j,k);
					particle.setMass(0.15);
					particles.push_back(particle);
				}
			}
		}
	}
}
