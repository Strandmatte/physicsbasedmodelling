#pragma once
#ifndef FOUCAULTPENDULUM_H
#define FOUCAULTPENDULUM_H

#include <QWidget>
#include <vector>
#include <QtOpenGL/QGLWidget>
#include "particle.h"

using namespace std;

class foucaultPendulum : public QObject{

	Q_OBJECT

public:
	foucaultPendulum();
	~foucaultPendulum();

	void paint();

	float dt;
	float g;
	float l;
	float T;
	float lat;
	int integrator;

	std::vector<std::vector<float>> path;

	Particle m;

	public slots:
		void reset();
		void step();

signals:
		void callUpdate();

};

#endif //FOUCAULTPENDULUM_H