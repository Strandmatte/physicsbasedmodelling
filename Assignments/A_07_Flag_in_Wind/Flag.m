clear
% Flag properties
nx = 16;
ny = 9;
dt = 0.02;
t_max = 10;

%spring props
kx = 1000
ky = 1000
kd = 300

%Wind force
Fx = @(t) 10.0;%2*(1+sin(t*2));
Fy = @(t) 0;
Fz = @(t) -5;

% particle init
X_init = zeros(nx*ny,3);
X = zeros(nx*ny,3);
V = zeros(nx*ny,3);
X0 = zeros(nx*ny,3);
V0 = zeros(nx*ny,3);

% spring matrix
Kx = zeros(nx*ny);
Ky = zeros(nx*ny);
Kd1 = zeros(nx*ny);
Kd2 = zeros(nx*ny);
% array linear index
idx = @(i,j) (j-1)*nx+i;

% initialize positions
%================================================================
for i = 1:nx
    for j=1:ny
        X(idx(i,j),1) = i; % set x  
        X(idx(i,j),2) = j; % set y  
        X(idx(i,j),3) = 0; % set z
    end
end
X_init = X;
X0 = X;


% initialize springs
%================================================================

% Horizontal Springs 
for i = 1:nx
    for j=1:ny
        
        %center case
        if i>1 && i<nx
            Kx(idx(i,j),idx(i-1,j)) = kx;              % - left  spring
            Kx(idx(i,j),idx(i,j)) = -kx;               % - right  spring
        
        % left border
        elseif i==1
            Kx(idx(i,j),idx(i,j)) = -kx;               % - right  spring
        
        %right border
        elseif i==nx
            Kx(idx(i,j),idx(i-1,j)) = kx;              % - left spring
        end
    end
end

% Vertical Springs 
for i = 1:nx
    for j=1:ny
        
        %center case
        if j>1 && j<ny
            Ky(idx(i,j),idx(i,j-1)) = ky;              % - upper spring
            Ky(idx(i,j),idx(i,j)) = -ky;               % - lower spring
        
        % top border
        elseif j==1
            Ky(idx(i,j),idx(i,j)) = -ky;               % - lower spring
        
        %bottom border
        elseif j==ny
            Ky(idx(i,j),idx(i,j-1)) = ky;              % - upper spring
        end
    end
end

% Diagonal Springs \
for i = 1:nx
    for j=1:ny
        %center case
        if j<ny && i<nx
            Kd1(idx(i,j),idx(i,j)) = -kd;               % - lower right spring
        end
        
        if j>1 && i>1
            Kd1(idx(i,j),idx(i-1,j-1)) = kd;            % - upper left spring
        end
    end
end

% Diagonal Springs /
for i = 1:nx
    for j=1:ny
        %center case
        if j<ny && i>1
            Kd2(idx(i,j),idx(i-1,j)) = -kd;             % - lower left spring
        end
        
        if j>1 && i<nx
            Kd2(idx(i,j),idx(i,j-1)) = kd;              % - upper right spring
        end
    end
end

% initialize figure render
%================================================================
figure('Renderer','zbuffer')
[sx,sy,sz] = sphere;
for i = 1:nx
    for j=1:ny
        surf(sx+3*X(idx(i,j),1),sy+3*X(idx(i,j),3),sz+3*X(idx(i,j),2),'FaceColor','red','EdgeColor','none')
        hold on 
    end
end
daspect([1 1 1])
axis tight manual;
set(gca,'NextPlot','replaceChildren');



%distances (spring elongations)
%====================
dist_h = ones(ny*nx,3);
dist_v = dist_h;
dist_d1 = zeros(ny*nx,3);
dist_d2 = zeros(ny*nx,3);
dist_h = dist_h*3.3333;

h0 = 1;
v0 = 1;
d0 = sqrt(2);

    
% time stepping
%================================================================
iter = 0;
for t = 1:dt:t_max
    iter = iter +1;
    
    
    %==================== Horizontal
    for x=1:nx-1
        for y=1:ny
            d = X(idx(x+1,y),:) - X(idx(x,y),:);
            elong = d-(d/norm(d))*h0;
            dist_h(idx(x,y),:) = elong ;
        end
    end
    %==================== Vertical
    for k=1:nx 
        for l=1:ny-1
            d=X(idx(k,l+1),:) - X(idx(k,l),:) ;
            elong = d-(d/norm(d))*v0;
            dist_v(idx(k,l),:) = elong ;
        end
    end
    %==================== Diagonal 1
    for k=1:nx-1
        for l=1:ny-1
            d = X(idx(k+1,l+1),:) - X(idx(k,l),:);
            elong = d-(d/norm(d))*d0;
            dist_d1(idx(k,l),:) = elong;
        end
    end
    %==================== Diagonal 2
    for k=2:nx
        for l=2:ny
            d = X(idx(k,l),:) - X(idx(k-1,l-1),:);
            elong = d-(d/norm(d))*d0;
            dist_d2(idx(k,l),:) = elong;
        end
    end
    
    %dist_h
    %dist_v;
    dist_d1(:,1);
    
    % update in X direction
    V(:,1) = V0(:,1) + dt*Fx(t);                            % Wind Force
    V(:,1) = V(:,1)  + dt*Kx*(-dist_h(:,1));            % Horizontal Springs
    V(:,1) = V(:,1)  + dt*Ky*(-dist_v(:,1));             % Vertical Springs
    V(:,1) = V(:,1)  + dt*Kd1*(-dist_d1(:,1));         % diagonal / Springs
    %V(:,1) = V(:,1)  + dt*Kd2*(-dist_d2(:,1));        % diagonal \ Springs
    X(:,1) = X0(:,1) + dt*V(:,1);
    
    % update in Y direction
    V(:,2) = V0(:,2) + dt*Fy(t);                            % Wind Force
    V(:,2) = V(:,2)  + dt*Kx*(-dist_h(:,2));             % Horizontal Springs
    V(:,2) = V(:,2)  + dt*Ky*(-dist_v(:,2));            % Vertical Springs
    V(:,2) = V(:,2)  + dt*Kd1*(-dist_d1(:,2));         % diagonal / Springs
    %V(:,2) = V(:,2)  + dt*Kd2*(-dist_d2(:,2));        % diagonal \ Springs
    X(:,2) = X0(:,2) + dt*V(:,2);
    
    % update in Z direction
    V(:,3) = V0(:,3) + dt*Fz(t);                            % Wind Force
    V(:,3) = V(:,3)  + dt*Kx*(-dist_h(:,3));          % Horizontal Springs
    V(:,3) = V(:,3)  + dt*Ky*(-dist_v(:,3));            % Vertical Springs
    V(:,3) = V(:,3)  + dt*Kd1*(-dist_d1(:,3));         % diagonal / Springs
    %V(:,3) = V(:,3)  + dt*Kd2*(-dist_d2(:,3));        % diagonal \ Springs
    X(:,3) = X0(:,3) + dt*V(:,3);
    
    
    %set fixed points
    for index=1:nx
    %     X(idx(index,1),:) = X_init(idx(index,1),:);
    end
    for index=1:ny
     %  X(idx(1,index),:) = X_init(idx(1,index),:);
    end
    
        X(idx(1,1),:) = X_init(idx(1,1),:);
        X(idx(1,ny),:) = X_init(idx(1,ny),:);
    V0 = V;
    X0 = X;
    
    xdata =[0;0;0];
    ydata =[0;0;0];
    zdata =[0;0;0];

    % redraw Flag
    %====================
    set(gca,'NextPlot','replaceChildren');
    for i = 1:nx
        for j=1:ny
            if(i<nx && j<ny)
                xdata = [xdata, [X(idx(i,j),1); X(idx(i+1,j),1); X(idx(i+1,j+1),1)] ];
                ydata = [ydata, [X(idx(i,j),3); X(idx(i+1,j),3); X(idx(i+1,j+1),3)] ];
                zdata = [zdata, [X(idx(i,j),2); X(idx(i+1,j),2); X(idx(i+1,j+1),2)] ];
                
                xdata = [xdata, [X(idx(i,j),1); X(idx(i+1,j+1),1); X(idx(i,j+1),1)] ];
                ydata = [ydata, [X(idx(i,j),3); X(idx(i+1,j+1),3); X(idx(i,j+1),3)] ];
                zdata = [zdata, [X(idx(i,j),2); X(idx(i+1,j+1),2); X(idx(i,j+1),2)] ];
            end
        end
    end
    
    [x,y,z] = sphere(2);
    surf(0*x,0*y,0*z,hadamard(2));
    shading interp;
    
    p = patch(3*xdata,3*ydata,3*zdata, 'g');
    set(p,'FaceColor','green','EdgeColor','black')
    hold on;
    daspect([1 1 1])
    camlight left; lighting phong;
    axis off;
    axis([-0 4*nx -3*nx nx 0 3*ny])
   
    
    F(iter) = getframe;
    t_max-t
    
    filename = 'flag_animation.gif';
    im = frame2im(F(iter));
    [A,map] = rgb2ind(im,256); 
	if iter == 1;
		imwrite(A,map,filename,'gif','LoopCount',Inf,'DelayTime',1);
	else
		imwrite(A,map,filename,'gif','WriteMode','append','DelayTime',0.04);
	end
    
end
% end time stepping
%================================================================


movie(F,2,25) % movie(F,n) = Play movie F, n times
