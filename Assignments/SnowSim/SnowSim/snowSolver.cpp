#include "snowSolver.h"
#include <iostream>
/*
*  snowSolver calculates the simulation based on the snowObject
*/

snowSolver::snowSolver(vector<snowObject*> obj){
	snowObjects = obj;
	init();
}
snowSolver::~snowSolver(){

}

void snowSolver::refresh(vector<snowObject*> obj){
	snowObjects.clear();
	snowObjects = obj;
	grid->~snowGrid();
	init();
}

void snowSolver::init(){

	//s.particles
	/*
		define grid with sice and spacing
		dynamic?
	*/

	float maxMass = -1;
	float mass;
	minPos(0) = 1000000;
	minPos(1) = 1000000;
	minPos(2) = 1000000;
	Eigen::Vector3f maxPos;
	maxPos(0) = -1000000;
	maxPos(1) = -1000000;
	maxPos(2) = -1000000;
	Eigen::Vector3f pos;

	//search for biggest particle and minimum/maximum coordinate
	for(int i = 0; i < snowObjects.size(); i++){
		for(int j = 0; j < snowObjects[i]->particles.size(); j++){
			pos = snowObjects[i]->particles[j].getPosition();
			mass = snowObjects[i]->particles[j].getMass();
			if(pos(0) < minPos(0)) minPos(0) = pos(0);
			if(pos(0) > maxPos(0)) maxPos(0) = pos(0);
			if(pos(1) < minPos(1)) minPos(1) = pos(1);
			if(pos(1) > maxPos(1)) maxPos(1) = pos(1);
			if(pos(2) < minPos(2)) minPos(2) = pos(2);
			if(pos(2) > maxPos(2)) maxPos(2) = pos(2);
			if(mass > maxMass) maxMass = mass;
		}
	}

	grid = new snowGrid(2*maxMass,2*maxMass,2*maxMass,ceil((maxPos(0)-minPos(0)+2*maxMass)/(2*maxMass)),ceil((maxPos(1)-minPos(1)+2*maxMass)/(2*maxMass)),ceil((maxPos(2)-minPos(2)+2*maxMass)/(2*maxMass)));

	// 1 Rasterize particle data to the grid
	rasterize();
	// if(timestep == 0)
		// 2 Compute particle volumes and densities.

}

void snowSolver::update(double dt){
	// 1 Rasterize particle data to the grid
	rasterize();
		//skip 2
	// 3 Compute grid forces

	// 4 Update velocities on grid

	// 5 Grid-based body collisions

	// 6 Solve linear system by integrating

	// 7 Update deformation gradient.

	// 8 Update particle velocities

	// 9 Particle-based body collisions

	// 10 Update particle positions

}

snowObject *snowSolver::getSnowObject(int pos){
	return snowObjects[pos];
}

void snowSolver::setDensities(float rho){
	for(int i = 0; i < snowObjects.size(); i++){
		for(int j = 0; j < snowObjects[i]->particles.size(); j++){
			snowObjects[i]->particles[j].setDensity(rho);
		}
	}
}

void snowSolver::rasterize(){

	Eigen::Vector3f pos;
	float m = 0.0f;
	float w = 0.0f;
	Eigen::Vector3f v;
	v << 0.0, 0.0, 0.0;

	// for all cells
	for(int i=0; i<grid->subdiv(0);i++){
		for(int j=0; j<grid->subdiv(1);j++){
			for(int k=0; k<grid->subdiv(2);k++){

				m=0.0f;
				v.setZero();

				// update mass
				for(int o = 0; o < snowObjects.size(); o++){
					for(int p = 0; p < snowObjects[o]->particles.size(); p++){
						pos = snowObjects[o]->particles[p].getPosition();

						w =			N( ( pos(0) - i*grid->cellsize(0) ) / grid->cellsize(0) )*
									N( ( pos(1) - j*grid->cellsize(1) ) / grid->cellsize(1) )*
									N( ( pos(2) - k*grid->cellsize(2) ) / grid->cellsize(2) );
						m += snowObjects[o]->particles[p].getMass() * w;
					}
				}
				grid->masses.at(grid->IX(i,j,k)) = m;


				// update velocity
				for(int o = 0; o < snowObjects.size(); o++){
					for(int p = 0; p < snowObjects[o]->particles.size(); p++){

						pos = snowObjects[o]->particles[p].getPosition();

						w =	N( ( pos(0) - i*grid->cellsize(0) ) / grid->cellsize(0) )*
							N( ( pos(1) - j*grid->cellsize(1) ) / grid->cellsize(1) )*
							N( ( pos(2) - k*grid->cellsize(2) ) / grid->cellsize(2) );

						if(grid->masses.at(grid->IX(i,j,k))!=0) // for not dividing by zero
							v += snowObjects[o]->particles[p].getVelocity() * snowObjects[o]->particles[p].getMass() * w / grid->masses.at(grid->IX(i,j,k));
					}
				}
				grid->velocities.at(grid->IX(i,j,k))(0) = v(0);
				grid->velocities.at(grid->IX(i,j,k))(1) = v(1);
				grid->velocities.at(grid->IX(i,j,k))(2) = v(2);
			}
		}
	}
}



float snowSolver::N(float x){

	if (abs(x)>=2)				// x is greater 2
		return 0.0;
	else if(abs(x)>=1)			// x is between 1 and 2
		return -(1.0/6.0)*pow(abs(x),3) + x*x - 2*abs(x) + (4.0/3.0);
	else						// x is between 0 and 1
		return (1.0/2.0)*pow(abs(x),3) - x*x + (2.0/3.0);

}
