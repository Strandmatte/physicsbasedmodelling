#include "hair.h"
#include <qdebug.h>

hair::hair()
{
	position.push_back(0);
	position.push_back(0);
	position.push_back(0);
	
	rotation.push_back(0);
	rotation.push_back(0);
	rotation.push_back(0);
}


hair::~hair()
{
}

void hair::init(){

	subdiv =20;
	// init cubes
	for(float i=0; i<subdiv; i+=0.1)
	{
		hairNode hnode_0;
		hnode_0.pos.at(0) = i+position[0];
		hnode_0.pos.at(1) = 0+position[1];
		hnode_0.pos.at(2) = 0+position[2];
		nodes.push_back(hnode_0);

		hairNode hnode_1;
		hnode_1.pos.at(0) = i+position[0];
		hnode_1.pos.at(1) = 0+position[1];
		hnode_1.pos.at(2) = 0.1+position[2];
		nodes.push_back(hnode_1);

		hairNode hnode_2;
		hnode_2.pos.at(0) = i+position[0];
		hnode_2.pos.at(1) = 0.1+position[1];
		hnode_2.pos.at(2) = 0+position[2];
		nodes.push_back(hnode_2);

		hairNode hnode_3;
		hnode_3.pos.at(0) = i+position[0];
		hnode_3.pos.at(1) = 0.1+position[1];
		hnode_3.pos.at(2) = 0.1+position[2];
		nodes.push_back(hnode_3);
	}

	// set static nodes
	for (int i=0; i<4; i++)
		nodes[i].dynamic = false;

	for(int i=0; i<subdiv-1; i++)
	{
		// for point 0
		HairSpring* horiz_spring_0 = new HairSpring();
		horiz_spring_0->setSpring(&nodes.at(i*4),&nodes.at(i*4+4));
		springs.push_back(*horiz_spring_0);

		HairSpring* depth_spring_0 = new HairSpring();
		depth_spring_0->setSpring(&nodes.at(i*4),&nodes.at(i*4+1));
		springs.push_back(*depth_spring_0);

		HairSpring* vert_spring_0 = new HairSpring();
		vert_spring_0->setSpring(&nodes.at(i*4),&nodes.at(i*4+2));
		springs.push_back(*vert_spring_0);

		HairSpring* diag_spring_0 = new HairSpring();
		diag_spring_0->setSpring(&nodes.at(i*4),&nodes.at(i*4+7));
		springs.push_back(*diag_spring_0);

		// for point 1
		HairSpring* horiz_spring_1 = new HairSpring();
		horiz_spring_1->setSpring(&nodes.at(i*4+1),&nodes.at(i*4+5));
		springs.push_back(*horiz_spring_1);

		HairSpring* diag_spring_1 = new HairSpring();
		diag_spring_1->setSpring(&nodes.at(i*4+1),&nodes.at(i*4+6));
		springs.push_back(*diag_spring_1);

		HairSpring* vert_spring_1 = new HairSpring();
		vert_spring_1->setSpring(&nodes.at(i*4+1),&nodes.at(i*4+3));
		springs.push_back(*vert_spring_1);

		// for point 2
		HairSpring* horiz_spring_2 = new HairSpring();
		horiz_spring_2->setSpring(&nodes.at(i*4+2),&nodes.at(i*4+6));
		springs.push_back(*horiz_spring_2);

		HairSpring* diag_spring_2 = new HairSpring();
		diag_spring_2->setSpring(&nodes.at(i*4+2),&nodes.at(i*4+5));
		springs.push_back(*diag_spring_2);

		HairSpring* depth_spring_2 = new HairSpring();
		depth_spring_2->setSpring(&nodes.at(i*4+2),&nodes.at(i*4+3));
		springs.push_back(*depth_spring_2);

		// for point 3
		HairSpring* horiz_spring_3 = new HairSpring();
		horiz_spring_3->setSpring(&nodes.at(i*4+3),&nodes.at(i*4+7));
		springs.push_back(*horiz_spring_3);

		HairSpring* diag_spring_3 = new HairSpring();
		diag_spring_3->setSpring(&nodes.at(i*4+3),&nodes.at(i*4+4));
		springs.push_back(*diag_spring_3);

	}
	for(int i=0; i<springs.size(); i++)
		springs[i].setZero();

}
