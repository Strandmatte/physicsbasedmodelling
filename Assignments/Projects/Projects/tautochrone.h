#ifndef SPLINE_H
#define SPLINE_H

#include <QWidget>
#include <vector>
#include <QtOpenGL/QGLWidget>
#include "particle.h"

using namespace std;

class Tautochrone : public QObject{

	Q_OBJECT

public:
	
	Tautochrone();
	Tautochrone(float pos);
	~Tautochrone();

	QPolygonF curve_tauto;

	float g;

	vector<Particle> particles;

	void init();
	void update();
	QPointF curve_function(float phi);
	void moveParticle(Particle *p);

	void paint();

	void dataOut();

	float t;
	float dt;
	float startPos;

	bool animating;

	public slots:
		void reset();
		void step();

signals:
		void callUpdate();
};

#endif // SPLINE_H
